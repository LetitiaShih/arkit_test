﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieMovement : MonoBehaviour {

	public float speed;
	[SerializeField] GameObject core;
	void Start () {
		core=GameObject.FindGameObjectWithTag("Core");
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt(core.transform);
		//transform.position = Vector3.MoveTowards(transform.position, core.transform.position, speed*Time.deltaTime);
		transform.Translate(Vector3.forward * speed * Time.deltaTime);
	}
	private void OnTriggerEnter(Collider other)
	{
		if(other.tag=="Arrow")
		{
			Destroy(gameObject,1f);
		}
	}
}
