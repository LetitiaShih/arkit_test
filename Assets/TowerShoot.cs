﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerShoot : MonoBehaviour {

	public float shootForce;
	GameObject arrow;
	public GameObject prefabArrow;
	Transform target;

	public float delay;
	public float countDown = 0;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(target!=null&& countDown < 0)
		{
			transform.LookAt(target);
			arrow=Instantiate(prefabArrow,transform.position,transform.rotation);
			arrow.GetComponent<Rigidbody>().AddForce(transform.forward*shootForce);
			Destroy(arrow,2);
			countDown=delay;
		}
		countDown -= Time.deltaTime;
		
	}
	private void OnTriggerStay(Collider other)
	{
		if(other.tag=="Zombie")
		{
			target=other.gameObject.transform;
		}
	}
}
