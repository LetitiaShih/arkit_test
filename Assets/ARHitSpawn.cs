﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityEngine.XR.IOS
{
	public class ARHitSpawn : MonoBehaviour
	{
		public float maxRayDistance = 30.0f;
		//ARKitPlane layer
		public LayerMask collisionLayer = 1 << 10;

		public GameObject toBeSpawned;
		public GameObject spawned;

		public bool started=false;
		public bool placed=false;
		void Start () {
		}

		// Update is called once per frame
		void Update () 
		{
			if (Input.GetMouseButtonDown (0) && placed == false)
			{
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				RaycastHit hit;
				if (Physics.Raycast (ray, out hit, maxRayDistance, collisionLayer)) 
				{
					spawned = Instantiate (toBeSpawned);
					Debug.Log(spawned.tag);
					spawned.transform.position = hit.point; //we're going to get the position from the contact point 
					spawned.transform.rotation = hit.transform.rotation; //and the rotation from the transform of the plane collider
					placed = true;
				}
			}
		}
			

		public GameObject prefabTower;
		public GameObject prefabZombie;
		public GameObject prefabCore;

		public void SpawnTower ()
		{
			toBeSpawned = prefabTower;
			placed = false;
		}

		public void SpawnZombie () 
		{
			toBeSpawned = prefabZombie;
			placed = false;
		}

		public void SpawnCore ()
		{
			if (started == false) 
			{
			toBeSpawned = prefabCore;
			placed = false;
			started = true;
			}
		}
	}

}