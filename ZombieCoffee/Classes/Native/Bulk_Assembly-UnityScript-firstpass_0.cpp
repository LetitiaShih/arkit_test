﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// Boo.Lang.GenericGeneratorEnumerator`1<System.Object>
struct GenericGeneratorEnumerator_1_t3544741914;
// Boo.Lang.GenericGenerator`1<System.Object>
struct GenericGenerator_1_t3943193262;
// CharacterMotor
struct CharacterMotor_t1911343696;
// CharacterMotor/$SubtractNewPlatformVelocity$1
struct U24SubtractNewPlatformVelocityU241_t3033866676;
// CharacterMotor/$SubtractNewPlatformVelocity$1/$
struct U24_t1451678995;
// CharacterMotorJumping
struct CharacterMotorJumping_t2963212952;
// CharacterMotorMovement
struct CharacterMotorMovement_t2204346181;
// CharacterMotorMovingPlatform
struct CharacterMotorMovingPlatform_t3582163828;
// CharacterMotorSliding
struct CharacterMotorSliding_t602719019;
// FPSInputController
struct FPSInputController_t1143113571;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t3512676632;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Void
struct Void_t1185182177;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.CharacterController
struct CharacterController_t1138636865;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t240592346;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1068524471;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.WaitForFixedUpdate
struct WaitForFixedUpdate_t1634918743;

extern RuntimeClass* AnimationCurve_t3046754366_il2cpp_TypeInfo_var;
extern RuntimeClass* CharacterController_t1138636865_il2cpp_TypeInfo_var;
extern RuntimeClass* CharacterMotorJumping_t2963212952_il2cpp_TypeInfo_var;
extern RuntimeClass* CharacterMotorMovement_t2204346181_il2cpp_TypeInfo_var;
extern RuntimeClass* CharacterMotorMovingPlatform_t3582163828_il2cpp_TypeInfo_var;
extern RuntimeClass* CharacterMotorSliding_t602719019_il2cpp_TypeInfo_var;
extern RuntimeClass* CharacterMotor_t1911343696_il2cpp_TypeInfo_var;
extern RuntimeClass* Input_t1431474628_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern RuntimeClass* KeyframeU5BU5D_t1068524471_il2cpp_TypeInfo_var;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* Quaternion_t2301928331_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* U24SubtractNewPlatformVelocityU241_t3033866676_il2cpp_TypeInfo_var;
extern RuntimeClass* U24_t1451678995_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern RuntimeClass* WaitForFixedUpdate_t1634918743_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1307271031;
extern String_t* _stringLiteral1828639942;
extern String_t* _stringLiteral1930566815;
extern String_t* _stringLiteral1959008414;
extern String_t* _stringLiteral2984908384;
extern String_t* _stringLiteral4082684263;
extern String_t* _stringLiteral919726132;
extern const RuntimeMethod* GenericGeneratorEnumerator_1_YieldDefault_m4013619751_RuntimeMethod_var;
extern const RuntimeMethod* GenericGeneratorEnumerator_1_Yield_m2645948398_RuntimeMethod_var;
extern const RuntimeMethod* GenericGeneratorEnumerator_1__ctor_m496418942_RuntimeMethod_var;
extern const RuntimeMethod* GenericGenerator_1__ctor_m599230948_RuntimeMethod_var;
extern const RuntimeType* CharacterController_t1138636865_0_0_0_var;
extern const RuntimeType* CharacterMotor_t1911343696_0_0_0_var;
extern const uint32_t CharacterMotorJumping__ctor_m2578071375_MetadataUsageId;
extern const uint32_t CharacterMotorMovement__ctor_m1785114476_MetadataUsageId;
extern const uint32_t CharacterMotor_AdjustGroundVelocityToNormal_m1559675676_MetadataUsageId;
extern const uint32_t CharacterMotor_ApplyGravityAndJumping_m3777966286_MetadataUsageId;
extern const uint32_t CharacterMotor_ApplyInputVelocityChange_m1754048112_MetadataUsageId;
extern const uint32_t CharacterMotor_Awake_m2777289438_MetadataUsageId;
extern const uint32_t CharacterMotor_CalculateJumpVerticalSpeed_m4078330566_MetadataUsageId;
extern const uint32_t CharacterMotor_FixedUpdate_m4065519527_MetadataUsageId;
extern const uint32_t CharacterMotor_GetDesiredHorizontalVelocity_m2565923170_MetadataUsageId;
extern const uint32_t CharacterMotor_MaxSpeedInDirection_m14659612_MetadataUsageId;
extern const uint32_t CharacterMotor_MoveWithPlatform_m2230124238_MetadataUsageId;
extern const uint32_t CharacterMotor_OnControllerColliderHit_m1457311545_MetadataUsageId;
extern const uint32_t CharacterMotor_SetVelocity_m726788142_MetadataUsageId;
extern const uint32_t CharacterMotor_SubtractNewPlatformVelocity_m2460956088_MetadataUsageId;
extern const uint32_t CharacterMotor_TooSteep_m1639249891_MetadataUsageId;
extern const uint32_t CharacterMotor_UpdateFunction_m3298730167_MetadataUsageId;
extern const uint32_t CharacterMotor__ctor_m2676047606_MetadataUsageId;
extern const uint32_t FPSInputController_Awake_m2261530822_MetadataUsageId;
extern const uint32_t FPSInputController_Update_m457713114_MetadataUsageId;
extern const uint32_t U24SubtractNewPlatformVelocityU241_GetEnumerator_m3891665188_MetadataUsageId;
extern const uint32_t U24SubtractNewPlatformVelocityU241__ctor_m2198776343_MetadataUsageId;
extern const uint32_t U24_MoveNext_m2202740849_MetadataUsageId;
extern const uint32_t U24__ctor_m4161687318_MetadataUsageId;

struct KeyframeU5BU5D_t1068524471;


#ifndef U3CMODULEU3E_T692745547_H
#define U3CMODULEU3E_T692745547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745547 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745547_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef GENERICGENERATORENUMERATOR_1_T3544741914_H
#define GENERICGENERATORENUMERATOR_1_T3544741914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.GenericGeneratorEnumerator`1<System.Object>
struct  GenericGeneratorEnumerator_1_t3544741914  : public RuntimeObject
{
public:
	// T Boo.Lang.GenericGeneratorEnumerator`1::_current
	RuntimeObject * ____current_0;
	// System.Int32 Boo.Lang.GenericGeneratorEnumerator`1::_state
	int32_t ____state_1;

public:
	inline static int32_t get_offset_of__current_0() { return static_cast<int32_t>(offsetof(GenericGeneratorEnumerator_1_t3544741914, ____current_0)); }
	inline RuntimeObject * get__current_0() const { return ____current_0; }
	inline RuntimeObject ** get_address_of__current_0() { return &____current_0; }
	inline void set__current_0(RuntimeObject * value)
	{
		____current_0 = value;
		Il2CppCodeGenWriteBarrier((&____current_0), value);
	}

	inline static int32_t get_offset_of__state_1() { return static_cast<int32_t>(offsetof(GenericGeneratorEnumerator_1_t3544741914, ____state_1)); }
	inline int32_t get__state_1() const { return ____state_1; }
	inline int32_t* get_address_of__state_1() { return &____state_1; }
	inline void set__state_1(int32_t value)
	{
		____state_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICGENERATORENUMERATOR_1_T3544741914_H
#ifndef GENERICGENERATOR_1_T3943193262_H
#define GENERICGENERATOR_1_T3943193262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.GenericGenerator`1<System.Object>
struct  GenericGenerator_1_t3943193262  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICGENERATOR_1_T3943193262_H
#ifndef CHARACTERMOTORSLIDING_T602719019_H
#define CHARACTERMOTORSLIDING_T602719019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterMotorSliding
struct  CharacterMotorSliding_t602719019  : public RuntimeObject
{
public:
	// System.Boolean CharacterMotorSliding::enabled
	bool ___enabled_0;
	// System.Single CharacterMotorSliding::slidingSpeed
	float ___slidingSpeed_1;
	// System.Single CharacterMotorSliding::sidewaysControl
	float ___sidewaysControl_2;
	// System.Single CharacterMotorSliding::speedControl
	float ___speedControl_3;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(CharacterMotorSliding_t602719019, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_slidingSpeed_1() { return static_cast<int32_t>(offsetof(CharacterMotorSliding_t602719019, ___slidingSpeed_1)); }
	inline float get_slidingSpeed_1() const { return ___slidingSpeed_1; }
	inline float* get_address_of_slidingSpeed_1() { return &___slidingSpeed_1; }
	inline void set_slidingSpeed_1(float value)
	{
		___slidingSpeed_1 = value;
	}

	inline static int32_t get_offset_of_sidewaysControl_2() { return static_cast<int32_t>(offsetof(CharacterMotorSliding_t602719019, ___sidewaysControl_2)); }
	inline float get_sidewaysControl_2() const { return ___sidewaysControl_2; }
	inline float* get_address_of_sidewaysControl_2() { return &___sidewaysControl_2; }
	inline void set_sidewaysControl_2(float value)
	{
		___sidewaysControl_2 = value;
	}

	inline static int32_t get_offset_of_speedControl_3() { return static_cast<int32_t>(offsetof(CharacterMotorSliding_t602719019, ___speedControl_3)); }
	inline float get_speedControl_3() const { return ___speedControl_3; }
	inline float* get_address_of_speedControl_3() { return &___speedControl_3; }
	inline void set_speedControl_3(float value)
	{
		___speedControl_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERMOTORSLIDING_T602719019_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef YIELDINSTRUCTION_T403091072_H
#define YIELDINSTRUCTION_T403091072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t403091072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T403091072_H
#ifndef U24SUBTRACTNEWPLATFORMVELOCITYU241_T3033866676_H
#define U24SUBTRACTNEWPLATFORMVELOCITYU241_T3033866676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterMotor/$SubtractNewPlatformVelocity$1
struct  U24SubtractNewPlatformVelocityU241_t3033866676  : public GenericGenerator_1_t3943193262
{
public:
	// CharacterMotor CharacterMotor/$SubtractNewPlatformVelocity$1::$self_$4
	CharacterMotor_t1911343696 * ___U24self_U244_0;

public:
	inline static int32_t get_offset_of_U24self_U244_0() { return static_cast<int32_t>(offsetof(U24SubtractNewPlatformVelocityU241_t3033866676, ___U24self_U244_0)); }
	inline CharacterMotor_t1911343696 * get_U24self_U244_0() const { return ___U24self_U244_0; }
	inline CharacterMotor_t1911343696 ** get_address_of_U24self_U244_0() { return &___U24self_U244_0; }
	inline void set_U24self_U244_0(CharacterMotor_t1911343696 * value)
	{
		___U24self_U244_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24self_U244_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24SUBTRACTNEWPLATFORMVELOCITYU241_T3033866676_H
#ifndef U24_T1451678995_H
#define U24_T1451678995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterMotor/$SubtractNewPlatformVelocity$1/$
struct  U24_t1451678995  : public GenericGeneratorEnumerator_1_t3544741914
{
public:
	// UnityEngine.Transform CharacterMotor/$SubtractNewPlatformVelocity$1/$::$platform$2
	Transform_t3600365921 * ___U24platformU242_2;
	// CharacterMotor CharacterMotor/$SubtractNewPlatformVelocity$1/$::$self_$3
	CharacterMotor_t1911343696 * ___U24self_U243_3;

public:
	inline static int32_t get_offset_of_U24platformU242_2() { return static_cast<int32_t>(offsetof(U24_t1451678995, ___U24platformU242_2)); }
	inline Transform_t3600365921 * get_U24platformU242_2() const { return ___U24platformU242_2; }
	inline Transform_t3600365921 ** get_address_of_U24platformU242_2() { return &___U24platformU242_2; }
	inline void set_U24platformU242_2(Transform_t3600365921 * value)
	{
		___U24platformU242_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24platformU242_2), value);
	}

	inline static int32_t get_offset_of_U24self_U243_3() { return static_cast<int32_t>(offsetof(U24_t1451678995, ___U24self_U243_3)); }
	inline CharacterMotor_t1911343696 * get_U24self_U243_3() const { return ___U24self_U243_3; }
	inline CharacterMotor_t1911343696 ** get_address_of_U24self_U243_3() { return &___U24self_U243_3; }
	inline void set_U24self_U243_3(CharacterMotor_t1911343696 * value)
	{
		___U24self_U243_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24self_U243_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24_T1451678995_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef KEYFRAME_T4206410242_H
#define KEYFRAME_T4206410242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Keyframe
struct  Keyframe_t4206410242 
{
public:
	// System.Single UnityEngine.Keyframe::m_Time
	float ___m_Time_0;
	// System.Single UnityEngine.Keyframe::m_Value
	float ___m_Value_1;
	// System.Single UnityEngine.Keyframe::m_InTangent
	float ___m_InTangent_2;
	// System.Single UnityEngine.Keyframe::m_OutTangent
	float ___m_OutTangent_3;
	// System.Int32 UnityEngine.Keyframe::m_WeightedMode
	int32_t ___m_WeightedMode_4;
	// System.Single UnityEngine.Keyframe::m_InWeight
	float ___m_InWeight_5;
	// System.Single UnityEngine.Keyframe::m_OutWeight
	float ___m_OutWeight_6;

public:
	inline static int32_t get_offset_of_m_Time_0() { return static_cast<int32_t>(offsetof(Keyframe_t4206410242, ___m_Time_0)); }
	inline float get_m_Time_0() const { return ___m_Time_0; }
	inline float* get_address_of_m_Time_0() { return &___m_Time_0; }
	inline void set_m_Time_0(float value)
	{
		___m_Time_0 = value;
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(Keyframe_t4206410242, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_m_InTangent_2() { return static_cast<int32_t>(offsetof(Keyframe_t4206410242, ___m_InTangent_2)); }
	inline float get_m_InTangent_2() const { return ___m_InTangent_2; }
	inline float* get_address_of_m_InTangent_2() { return &___m_InTangent_2; }
	inline void set_m_InTangent_2(float value)
	{
		___m_InTangent_2 = value;
	}

	inline static int32_t get_offset_of_m_OutTangent_3() { return static_cast<int32_t>(offsetof(Keyframe_t4206410242, ___m_OutTangent_3)); }
	inline float get_m_OutTangent_3() const { return ___m_OutTangent_3; }
	inline float* get_address_of_m_OutTangent_3() { return &___m_OutTangent_3; }
	inline void set_m_OutTangent_3(float value)
	{
		___m_OutTangent_3 = value;
	}

	inline static int32_t get_offset_of_m_WeightedMode_4() { return static_cast<int32_t>(offsetof(Keyframe_t4206410242, ___m_WeightedMode_4)); }
	inline int32_t get_m_WeightedMode_4() const { return ___m_WeightedMode_4; }
	inline int32_t* get_address_of_m_WeightedMode_4() { return &___m_WeightedMode_4; }
	inline void set_m_WeightedMode_4(int32_t value)
	{
		___m_WeightedMode_4 = value;
	}

	inline static int32_t get_offset_of_m_InWeight_5() { return static_cast<int32_t>(offsetof(Keyframe_t4206410242, ___m_InWeight_5)); }
	inline float get_m_InWeight_5() const { return ___m_InWeight_5; }
	inline float* get_address_of_m_InWeight_5() { return &___m_InWeight_5; }
	inline void set_m_InWeight_5(float value)
	{
		___m_InWeight_5 = value;
	}

	inline static int32_t get_offset_of_m_OutWeight_6() { return static_cast<int32_t>(offsetof(Keyframe_t4206410242, ___m_OutWeight_6)); }
	inline float get_m_OutWeight_6() const { return ___m_OutWeight_6; }
	inline float* get_address_of_m_OutWeight_6() { return &___m_OutWeight_6; }
	inline void set_m_OutWeight_6(float value)
	{
		___m_OutWeight_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYFRAME_T4206410242_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef WAITFORFIXEDUPDATE_T1634918743_H
#define WAITFORFIXEDUPDATE_T1634918743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForFixedUpdate
struct  WaitForFixedUpdate_t1634918743  : public YieldInstruction_t403091072
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORFIXEDUPDATE_T1634918743_H
#ifndef CHARACTERMOTORJUMPING_T2963212952_H
#define CHARACTERMOTORJUMPING_T2963212952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterMotorJumping
struct  CharacterMotorJumping_t2963212952  : public RuntimeObject
{
public:
	// System.Boolean CharacterMotorJumping::enabled
	bool ___enabled_0;
	// System.Single CharacterMotorJumping::baseHeight
	float ___baseHeight_1;
	// System.Single CharacterMotorJumping::extraHeight
	float ___extraHeight_2;
	// System.Single CharacterMotorJumping::perpAmount
	float ___perpAmount_3;
	// System.Single CharacterMotorJumping::steepPerpAmount
	float ___steepPerpAmount_4;
	// System.Boolean CharacterMotorJumping::jumping
	bool ___jumping_5;
	// System.Boolean CharacterMotorJumping::holdingJumpButton
	bool ___holdingJumpButton_6;
	// System.Single CharacterMotorJumping::lastStartTime
	float ___lastStartTime_7;
	// System.Single CharacterMotorJumping::lastButtonDownTime
	float ___lastButtonDownTime_8;
	// UnityEngine.Vector3 CharacterMotorJumping::jumpDir
	Vector3_t3722313464  ___jumpDir_9;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(CharacterMotorJumping_t2963212952, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_baseHeight_1() { return static_cast<int32_t>(offsetof(CharacterMotorJumping_t2963212952, ___baseHeight_1)); }
	inline float get_baseHeight_1() const { return ___baseHeight_1; }
	inline float* get_address_of_baseHeight_1() { return &___baseHeight_1; }
	inline void set_baseHeight_1(float value)
	{
		___baseHeight_1 = value;
	}

	inline static int32_t get_offset_of_extraHeight_2() { return static_cast<int32_t>(offsetof(CharacterMotorJumping_t2963212952, ___extraHeight_2)); }
	inline float get_extraHeight_2() const { return ___extraHeight_2; }
	inline float* get_address_of_extraHeight_2() { return &___extraHeight_2; }
	inline void set_extraHeight_2(float value)
	{
		___extraHeight_2 = value;
	}

	inline static int32_t get_offset_of_perpAmount_3() { return static_cast<int32_t>(offsetof(CharacterMotorJumping_t2963212952, ___perpAmount_3)); }
	inline float get_perpAmount_3() const { return ___perpAmount_3; }
	inline float* get_address_of_perpAmount_3() { return &___perpAmount_3; }
	inline void set_perpAmount_3(float value)
	{
		___perpAmount_3 = value;
	}

	inline static int32_t get_offset_of_steepPerpAmount_4() { return static_cast<int32_t>(offsetof(CharacterMotorJumping_t2963212952, ___steepPerpAmount_4)); }
	inline float get_steepPerpAmount_4() const { return ___steepPerpAmount_4; }
	inline float* get_address_of_steepPerpAmount_4() { return &___steepPerpAmount_4; }
	inline void set_steepPerpAmount_4(float value)
	{
		___steepPerpAmount_4 = value;
	}

	inline static int32_t get_offset_of_jumping_5() { return static_cast<int32_t>(offsetof(CharacterMotorJumping_t2963212952, ___jumping_5)); }
	inline bool get_jumping_5() const { return ___jumping_5; }
	inline bool* get_address_of_jumping_5() { return &___jumping_5; }
	inline void set_jumping_5(bool value)
	{
		___jumping_5 = value;
	}

	inline static int32_t get_offset_of_holdingJumpButton_6() { return static_cast<int32_t>(offsetof(CharacterMotorJumping_t2963212952, ___holdingJumpButton_6)); }
	inline bool get_holdingJumpButton_6() const { return ___holdingJumpButton_6; }
	inline bool* get_address_of_holdingJumpButton_6() { return &___holdingJumpButton_6; }
	inline void set_holdingJumpButton_6(bool value)
	{
		___holdingJumpButton_6 = value;
	}

	inline static int32_t get_offset_of_lastStartTime_7() { return static_cast<int32_t>(offsetof(CharacterMotorJumping_t2963212952, ___lastStartTime_7)); }
	inline float get_lastStartTime_7() const { return ___lastStartTime_7; }
	inline float* get_address_of_lastStartTime_7() { return &___lastStartTime_7; }
	inline void set_lastStartTime_7(float value)
	{
		___lastStartTime_7 = value;
	}

	inline static int32_t get_offset_of_lastButtonDownTime_8() { return static_cast<int32_t>(offsetof(CharacterMotorJumping_t2963212952, ___lastButtonDownTime_8)); }
	inline float get_lastButtonDownTime_8() const { return ___lastButtonDownTime_8; }
	inline float* get_address_of_lastButtonDownTime_8() { return &___lastButtonDownTime_8; }
	inline void set_lastButtonDownTime_8(float value)
	{
		___lastButtonDownTime_8 = value;
	}

	inline static int32_t get_offset_of_jumpDir_9() { return static_cast<int32_t>(offsetof(CharacterMotorJumping_t2963212952, ___jumpDir_9)); }
	inline Vector3_t3722313464  get_jumpDir_9() const { return ___jumpDir_9; }
	inline Vector3_t3722313464 * get_address_of_jumpDir_9() { return &___jumpDir_9; }
	inline void set_jumpDir_9(Vector3_t3722313464  value)
	{
		___jumpDir_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERMOTORJUMPING_T2963212952_H
#ifndef MOVEMENTTRANSFERONJUMP_T1874953612_H
#define MOVEMENTTRANSFERONJUMP_T1874953612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MovementTransferOnJump
struct  MovementTransferOnJump_t1874953612 
{
public:
	// System.Int32 MovementTransferOnJump::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MovementTransferOnJump_t1874953612, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENTTRANSFERONJUMP_T1874953612_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
#ifndef ANIMATIONCURVE_T3046754366_H
#define ANIMATIONCURVE_T3046754366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationCurve
struct  AnimationCurve_t3046754366  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t3046754366, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // ANIMATIONCURVE_T3046754366_H
#ifndef COLLISIONFLAGS_T1776808576_H
#define COLLISIONFLAGS_T1776808576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CollisionFlags
struct  CollisionFlags_t1776808576 
{
public:
	// System.Int32 UnityEngine.CollisionFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CollisionFlags_t1776808576, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISIONFLAGS_T1776808576_H
#ifndef CONTROLLERCOLLIDERHIT_T240592346_H
#define CONTROLLERCOLLIDERHIT_T240592346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ControllerColliderHit
struct  ControllerColliderHit_t240592346  : public RuntimeObject
{
public:
	// UnityEngine.CharacterController UnityEngine.ControllerColliderHit::m_Controller
	CharacterController_t1138636865 * ___m_Controller_0;
	// UnityEngine.Collider UnityEngine.ControllerColliderHit::m_Collider
	Collider_t1773347010 * ___m_Collider_1;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_Point
	Vector3_t3722313464  ___m_Point_2;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_Normal
	Vector3_t3722313464  ___m_Normal_3;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_MoveDirection
	Vector3_t3722313464  ___m_MoveDirection_4;
	// System.Single UnityEngine.ControllerColliderHit::m_MoveLength
	float ___m_MoveLength_5;
	// System.Int32 UnityEngine.ControllerColliderHit::m_Push
	int32_t ___m_Push_6;

public:
	inline static int32_t get_offset_of_m_Controller_0() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_Controller_0)); }
	inline CharacterController_t1138636865 * get_m_Controller_0() const { return ___m_Controller_0; }
	inline CharacterController_t1138636865 ** get_address_of_m_Controller_0() { return &___m_Controller_0; }
	inline void set_m_Controller_0(CharacterController_t1138636865 * value)
	{
		___m_Controller_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Controller_0), value);
	}

	inline static int32_t get_offset_of_m_Collider_1() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_Collider_1)); }
	inline Collider_t1773347010 * get_m_Collider_1() const { return ___m_Collider_1; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_1() { return &___m_Collider_1; }
	inline void set_m_Collider_1(Collider_t1773347010 * value)
	{
		___m_Collider_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_1), value);
	}

	inline static int32_t get_offset_of_m_Point_2() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_Point_2)); }
	inline Vector3_t3722313464  get_m_Point_2() const { return ___m_Point_2; }
	inline Vector3_t3722313464 * get_address_of_m_Point_2() { return &___m_Point_2; }
	inline void set_m_Point_2(Vector3_t3722313464  value)
	{
		___m_Point_2 = value;
	}

	inline static int32_t get_offset_of_m_Normal_3() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_Normal_3)); }
	inline Vector3_t3722313464  get_m_Normal_3() const { return ___m_Normal_3; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_3() { return &___m_Normal_3; }
	inline void set_m_Normal_3(Vector3_t3722313464  value)
	{
		___m_Normal_3 = value;
	}

	inline static int32_t get_offset_of_m_MoveDirection_4() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_MoveDirection_4)); }
	inline Vector3_t3722313464  get_m_MoveDirection_4() const { return ___m_MoveDirection_4; }
	inline Vector3_t3722313464 * get_address_of_m_MoveDirection_4() { return &___m_MoveDirection_4; }
	inline void set_m_MoveDirection_4(Vector3_t3722313464  value)
	{
		___m_MoveDirection_4 = value;
	}

	inline static int32_t get_offset_of_m_MoveLength_5() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_MoveLength_5)); }
	inline float get_m_MoveLength_5() const { return ___m_MoveLength_5; }
	inline float* get_address_of_m_MoveLength_5() { return &___m_MoveLength_5; }
	inline void set_m_MoveLength_5(float value)
	{
		___m_MoveLength_5 = value;
	}

	inline static int32_t get_offset_of_m_Push_6() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_Push_6)); }
	inline int32_t get_m_Push_6() const { return ___m_Push_6; }
	inline int32_t* get_address_of_m_Push_6() { return &___m_Push_6; }
	inline void set_m_Push_6(int32_t value)
	{
		___m_Push_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t240592346_marshaled_pinvoke
{
	CharacterController_t1138636865 * ___m_Controller_0;
	Collider_t1773347010 * ___m_Collider_1;
	Vector3_t3722313464  ___m_Point_2;
	Vector3_t3722313464  ___m_Normal_3;
	Vector3_t3722313464  ___m_MoveDirection_4;
	float ___m_MoveLength_5;
	int32_t ___m_Push_6;
};
// Native definition for COM marshalling of UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t240592346_marshaled_com
{
	CharacterController_t1138636865 * ___m_Controller_0;
	Collider_t1773347010 * ___m_Collider_1;
	Vector3_t3722313464  ___m_Point_2;
	Vector3_t3722313464  ___m_Normal_3;
	Vector3_t3722313464  ___m_MoveDirection_4;
	float ___m_MoveLength_5;
	int32_t ___m_Push_6;
};
#endif // CONTROLLERCOLLIDERHIT_T240592346_H
#ifndef COROUTINE_T3829159415_H
#define COROUTINE_T3829159415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t3829159415  : public YieldInstruction_t403091072
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t3829159415, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T3829159415_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef SENDMESSAGEOPTIONS_T3580193095_H
#define SENDMESSAGEOPTIONS_T3580193095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SendMessageOptions
struct  SendMessageOptions_t3580193095 
{
public:
	// System.Int32 UnityEngine.SendMessageOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SendMessageOptions_t3580193095, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDMESSAGEOPTIONS_T3580193095_H
#ifndef CHARACTERMOTORMOVEMENT_T2204346181_H
#define CHARACTERMOTORMOVEMENT_T2204346181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterMotorMovement
struct  CharacterMotorMovement_t2204346181  : public RuntimeObject
{
public:
	// System.Single CharacterMotorMovement::maxForwardSpeed
	float ___maxForwardSpeed_0;
	// System.Single CharacterMotorMovement::maxSidewaysSpeed
	float ___maxSidewaysSpeed_1;
	// System.Single CharacterMotorMovement::maxBackwardsSpeed
	float ___maxBackwardsSpeed_2;
	// UnityEngine.AnimationCurve CharacterMotorMovement::slopeSpeedMultiplier
	AnimationCurve_t3046754366 * ___slopeSpeedMultiplier_3;
	// System.Single CharacterMotorMovement::maxGroundAcceleration
	float ___maxGroundAcceleration_4;
	// System.Single CharacterMotorMovement::maxAirAcceleration
	float ___maxAirAcceleration_5;
	// System.Single CharacterMotorMovement::gravity
	float ___gravity_6;
	// System.Single CharacterMotorMovement::maxFallSpeed
	float ___maxFallSpeed_7;
	// UnityEngine.CollisionFlags CharacterMotorMovement::collisionFlags
	int32_t ___collisionFlags_8;
	// UnityEngine.Vector3 CharacterMotorMovement::velocity
	Vector3_t3722313464  ___velocity_9;
	// UnityEngine.Vector3 CharacterMotorMovement::frameVelocity
	Vector3_t3722313464  ___frameVelocity_10;
	// UnityEngine.Vector3 CharacterMotorMovement::hitPoint
	Vector3_t3722313464  ___hitPoint_11;
	// UnityEngine.Vector3 CharacterMotorMovement::lastHitPoint
	Vector3_t3722313464  ___lastHitPoint_12;

public:
	inline static int32_t get_offset_of_maxForwardSpeed_0() { return static_cast<int32_t>(offsetof(CharacterMotorMovement_t2204346181, ___maxForwardSpeed_0)); }
	inline float get_maxForwardSpeed_0() const { return ___maxForwardSpeed_0; }
	inline float* get_address_of_maxForwardSpeed_0() { return &___maxForwardSpeed_0; }
	inline void set_maxForwardSpeed_0(float value)
	{
		___maxForwardSpeed_0 = value;
	}

	inline static int32_t get_offset_of_maxSidewaysSpeed_1() { return static_cast<int32_t>(offsetof(CharacterMotorMovement_t2204346181, ___maxSidewaysSpeed_1)); }
	inline float get_maxSidewaysSpeed_1() const { return ___maxSidewaysSpeed_1; }
	inline float* get_address_of_maxSidewaysSpeed_1() { return &___maxSidewaysSpeed_1; }
	inline void set_maxSidewaysSpeed_1(float value)
	{
		___maxSidewaysSpeed_1 = value;
	}

	inline static int32_t get_offset_of_maxBackwardsSpeed_2() { return static_cast<int32_t>(offsetof(CharacterMotorMovement_t2204346181, ___maxBackwardsSpeed_2)); }
	inline float get_maxBackwardsSpeed_2() const { return ___maxBackwardsSpeed_2; }
	inline float* get_address_of_maxBackwardsSpeed_2() { return &___maxBackwardsSpeed_2; }
	inline void set_maxBackwardsSpeed_2(float value)
	{
		___maxBackwardsSpeed_2 = value;
	}

	inline static int32_t get_offset_of_slopeSpeedMultiplier_3() { return static_cast<int32_t>(offsetof(CharacterMotorMovement_t2204346181, ___slopeSpeedMultiplier_3)); }
	inline AnimationCurve_t3046754366 * get_slopeSpeedMultiplier_3() const { return ___slopeSpeedMultiplier_3; }
	inline AnimationCurve_t3046754366 ** get_address_of_slopeSpeedMultiplier_3() { return &___slopeSpeedMultiplier_3; }
	inline void set_slopeSpeedMultiplier_3(AnimationCurve_t3046754366 * value)
	{
		___slopeSpeedMultiplier_3 = value;
		Il2CppCodeGenWriteBarrier((&___slopeSpeedMultiplier_3), value);
	}

	inline static int32_t get_offset_of_maxGroundAcceleration_4() { return static_cast<int32_t>(offsetof(CharacterMotorMovement_t2204346181, ___maxGroundAcceleration_4)); }
	inline float get_maxGroundAcceleration_4() const { return ___maxGroundAcceleration_4; }
	inline float* get_address_of_maxGroundAcceleration_4() { return &___maxGroundAcceleration_4; }
	inline void set_maxGroundAcceleration_4(float value)
	{
		___maxGroundAcceleration_4 = value;
	}

	inline static int32_t get_offset_of_maxAirAcceleration_5() { return static_cast<int32_t>(offsetof(CharacterMotorMovement_t2204346181, ___maxAirAcceleration_5)); }
	inline float get_maxAirAcceleration_5() const { return ___maxAirAcceleration_5; }
	inline float* get_address_of_maxAirAcceleration_5() { return &___maxAirAcceleration_5; }
	inline void set_maxAirAcceleration_5(float value)
	{
		___maxAirAcceleration_5 = value;
	}

	inline static int32_t get_offset_of_gravity_6() { return static_cast<int32_t>(offsetof(CharacterMotorMovement_t2204346181, ___gravity_6)); }
	inline float get_gravity_6() const { return ___gravity_6; }
	inline float* get_address_of_gravity_6() { return &___gravity_6; }
	inline void set_gravity_6(float value)
	{
		___gravity_6 = value;
	}

	inline static int32_t get_offset_of_maxFallSpeed_7() { return static_cast<int32_t>(offsetof(CharacterMotorMovement_t2204346181, ___maxFallSpeed_7)); }
	inline float get_maxFallSpeed_7() const { return ___maxFallSpeed_7; }
	inline float* get_address_of_maxFallSpeed_7() { return &___maxFallSpeed_7; }
	inline void set_maxFallSpeed_7(float value)
	{
		___maxFallSpeed_7 = value;
	}

	inline static int32_t get_offset_of_collisionFlags_8() { return static_cast<int32_t>(offsetof(CharacterMotorMovement_t2204346181, ___collisionFlags_8)); }
	inline int32_t get_collisionFlags_8() const { return ___collisionFlags_8; }
	inline int32_t* get_address_of_collisionFlags_8() { return &___collisionFlags_8; }
	inline void set_collisionFlags_8(int32_t value)
	{
		___collisionFlags_8 = value;
	}

	inline static int32_t get_offset_of_velocity_9() { return static_cast<int32_t>(offsetof(CharacterMotorMovement_t2204346181, ___velocity_9)); }
	inline Vector3_t3722313464  get_velocity_9() const { return ___velocity_9; }
	inline Vector3_t3722313464 * get_address_of_velocity_9() { return &___velocity_9; }
	inline void set_velocity_9(Vector3_t3722313464  value)
	{
		___velocity_9 = value;
	}

	inline static int32_t get_offset_of_frameVelocity_10() { return static_cast<int32_t>(offsetof(CharacterMotorMovement_t2204346181, ___frameVelocity_10)); }
	inline Vector3_t3722313464  get_frameVelocity_10() const { return ___frameVelocity_10; }
	inline Vector3_t3722313464 * get_address_of_frameVelocity_10() { return &___frameVelocity_10; }
	inline void set_frameVelocity_10(Vector3_t3722313464  value)
	{
		___frameVelocity_10 = value;
	}

	inline static int32_t get_offset_of_hitPoint_11() { return static_cast<int32_t>(offsetof(CharacterMotorMovement_t2204346181, ___hitPoint_11)); }
	inline Vector3_t3722313464  get_hitPoint_11() const { return ___hitPoint_11; }
	inline Vector3_t3722313464 * get_address_of_hitPoint_11() { return &___hitPoint_11; }
	inline void set_hitPoint_11(Vector3_t3722313464  value)
	{
		___hitPoint_11 = value;
	}

	inline static int32_t get_offset_of_lastHitPoint_12() { return static_cast<int32_t>(offsetof(CharacterMotorMovement_t2204346181, ___lastHitPoint_12)); }
	inline Vector3_t3722313464  get_lastHitPoint_12() const { return ___lastHitPoint_12; }
	inline Vector3_t3722313464 * get_address_of_lastHitPoint_12() { return &___lastHitPoint_12; }
	inline void set_lastHitPoint_12(Vector3_t3722313464  value)
	{
		___lastHitPoint_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERMOTORMOVEMENT_T2204346181_H
#ifndef CHARACTERMOTORMOVINGPLATFORM_T3582163828_H
#define CHARACTERMOTORMOVINGPLATFORM_T3582163828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterMotorMovingPlatform
struct  CharacterMotorMovingPlatform_t3582163828  : public RuntimeObject
{
public:
	// System.Boolean CharacterMotorMovingPlatform::enabled
	bool ___enabled_0;
	// MovementTransferOnJump CharacterMotorMovingPlatform::movementTransfer
	int32_t ___movementTransfer_1;
	// UnityEngine.Transform CharacterMotorMovingPlatform::hitPlatform
	Transform_t3600365921 * ___hitPlatform_2;
	// UnityEngine.Transform CharacterMotorMovingPlatform::activePlatform
	Transform_t3600365921 * ___activePlatform_3;
	// UnityEngine.Vector3 CharacterMotorMovingPlatform::activeLocalPoint
	Vector3_t3722313464  ___activeLocalPoint_4;
	// UnityEngine.Vector3 CharacterMotorMovingPlatform::activeGlobalPoint
	Vector3_t3722313464  ___activeGlobalPoint_5;
	// UnityEngine.Quaternion CharacterMotorMovingPlatform::activeLocalRotation
	Quaternion_t2301928331  ___activeLocalRotation_6;
	// UnityEngine.Quaternion CharacterMotorMovingPlatform::activeGlobalRotation
	Quaternion_t2301928331  ___activeGlobalRotation_7;
	// UnityEngine.Matrix4x4 CharacterMotorMovingPlatform::lastMatrix
	Matrix4x4_t1817901843  ___lastMatrix_8;
	// UnityEngine.Vector3 CharacterMotorMovingPlatform::platformVelocity
	Vector3_t3722313464  ___platformVelocity_9;
	// System.Boolean CharacterMotorMovingPlatform::newPlatform
	bool ___newPlatform_10;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(CharacterMotorMovingPlatform_t3582163828, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_movementTransfer_1() { return static_cast<int32_t>(offsetof(CharacterMotorMovingPlatform_t3582163828, ___movementTransfer_1)); }
	inline int32_t get_movementTransfer_1() const { return ___movementTransfer_1; }
	inline int32_t* get_address_of_movementTransfer_1() { return &___movementTransfer_1; }
	inline void set_movementTransfer_1(int32_t value)
	{
		___movementTransfer_1 = value;
	}

	inline static int32_t get_offset_of_hitPlatform_2() { return static_cast<int32_t>(offsetof(CharacterMotorMovingPlatform_t3582163828, ___hitPlatform_2)); }
	inline Transform_t3600365921 * get_hitPlatform_2() const { return ___hitPlatform_2; }
	inline Transform_t3600365921 ** get_address_of_hitPlatform_2() { return &___hitPlatform_2; }
	inline void set_hitPlatform_2(Transform_t3600365921 * value)
	{
		___hitPlatform_2 = value;
		Il2CppCodeGenWriteBarrier((&___hitPlatform_2), value);
	}

	inline static int32_t get_offset_of_activePlatform_3() { return static_cast<int32_t>(offsetof(CharacterMotorMovingPlatform_t3582163828, ___activePlatform_3)); }
	inline Transform_t3600365921 * get_activePlatform_3() const { return ___activePlatform_3; }
	inline Transform_t3600365921 ** get_address_of_activePlatform_3() { return &___activePlatform_3; }
	inline void set_activePlatform_3(Transform_t3600365921 * value)
	{
		___activePlatform_3 = value;
		Il2CppCodeGenWriteBarrier((&___activePlatform_3), value);
	}

	inline static int32_t get_offset_of_activeLocalPoint_4() { return static_cast<int32_t>(offsetof(CharacterMotorMovingPlatform_t3582163828, ___activeLocalPoint_4)); }
	inline Vector3_t3722313464  get_activeLocalPoint_4() const { return ___activeLocalPoint_4; }
	inline Vector3_t3722313464 * get_address_of_activeLocalPoint_4() { return &___activeLocalPoint_4; }
	inline void set_activeLocalPoint_4(Vector3_t3722313464  value)
	{
		___activeLocalPoint_4 = value;
	}

	inline static int32_t get_offset_of_activeGlobalPoint_5() { return static_cast<int32_t>(offsetof(CharacterMotorMovingPlatform_t3582163828, ___activeGlobalPoint_5)); }
	inline Vector3_t3722313464  get_activeGlobalPoint_5() const { return ___activeGlobalPoint_5; }
	inline Vector3_t3722313464 * get_address_of_activeGlobalPoint_5() { return &___activeGlobalPoint_5; }
	inline void set_activeGlobalPoint_5(Vector3_t3722313464  value)
	{
		___activeGlobalPoint_5 = value;
	}

	inline static int32_t get_offset_of_activeLocalRotation_6() { return static_cast<int32_t>(offsetof(CharacterMotorMovingPlatform_t3582163828, ___activeLocalRotation_6)); }
	inline Quaternion_t2301928331  get_activeLocalRotation_6() const { return ___activeLocalRotation_6; }
	inline Quaternion_t2301928331 * get_address_of_activeLocalRotation_6() { return &___activeLocalRotation_6; }
	inline void set_activeLocalRotation_6(Quaternion_t2301928331  value)
	{
		___activeLocalRotation_6 = value;
	}

	inline static int32_t get_offset_of_activeGlobalRotation_7() { return static_cast<int32_t>(offsetof(CharacterMotorMovingPlatform_t3582163828, ___activeGlobalRotation_7)); }
	inline Quaternion_t2301928331  get_activeGlobalRotation_7() const { return ___activeGlobalRotation_7; }
	inline Quaternion_t2301928331 * get_address_of_activeGlobalRotation_7() { return &___activeGlobalRotation_7; }
	inline void set_activeGlobalRotation_7(Quaternion_t2301928331  value)
	{
		___activeGlobalRotation_7 = value;
	}

	inline static int32_t get_offset_of_lastMatrix_8() { return static_cast<int32_t>(offsetof(CharacterMotorMovingPlatform_t3582163828, ___lastMatrix_8)); }
	inline Matrix4x4_t1817901843  get_lastMatrix_8() const { return ___lastMatrix_8; }
	inline Matrix4x4_t1817901843 * get_address_of_lastMatrix_8() { return &___lastMatrix_8; }
	inline void set_lastMatrix_8(Matrix4x4_t1817901843  value)
	{
		___lastMatrix_8 = value;
	}

	inline static int32_t get_offset_of_platformVelocity_9() { return static_cast<int32_t>(offsetof(CharacterMotorMovingPlatform_t3582163828, ___platformVelocity_9)); }
	inline Vector3_t3722313464  get_platformVelocity_9() const { return ___platformVelocity_9; }
	inline Vector3_t3722313464 * get_address_of_platformVelocity_9() { return &___platformVelocity_9; }
	inline void set_platformVelocity_9(Vector3_t3722313464  value)
	{
		___platformVelocity_9 = value;
	}

	inline static int32_t get_offset_of_newPlatform_10() { return static_cast<int32_t>(offsetof(CharacterMotorMovingPlatform_t3582163828, ___newPlatform_10)); }
	inline bool get_newPlatform_10() const { return ___newPlatform_10; }
	inline bool* get_address_of_newPlatform_10() { return &___newPlatform_10; }
	inline void set_newPlatform_10(bool value)
	{
		___newPlatform_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERMOTORMOVINGPLATFORM_T3582163828_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t426314064 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t426314064 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef COLLIDER_T1773347010_H
#define COLLIDER_T1773347010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider
struct  Collider_t1773347010  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER_T1773347010_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:
	// System.Int32 UnityEngine.Transform::<hierarchyCount>k__BackingField
	int32_t ___U3ChierarchyCountU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3ChierarchyCountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Transform_t3600365921, ___U3ChierarchyCountU3Ek__BackingField_4)); }
	inline int32_t get_U3ChierarchyCountU3Ek__BackingField_4() const { return ___U3ChierarchyCountU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3ChierarchyCountU3Ek__BackingField_4() { return &___U3ChierarchyCountU3Ek__BackingField_4; }
	inline void set_U3ChierarchyCountU3Ek__BackingField_4(int32_t value)
	{
		___U3ChierarchyCountU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef CHARACTERCONTROLLER_T1138636865_H
#define CHARACTERCONTROLLER_T1138636865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CharacterController
struct  CharacterController_t1138636865  : public Collider_t1773347010
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERCONTROLLER_T1138636865_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef CHARACTERMOTOR_T1911343696_H
#define CHARACTERMOTOR_T1911343696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterMotor
struct  CharacterMotor_t1911343696  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean CharacterMotor::canControl
	bool ___canControl_4;
	// System.Boolean CharacterMotor::useFixedUpdate
	bool ___useFixedUpdate_5;
	// UnityEngine.Vector3 CharacterMotor::inputMoveDirection
	Vector3_t3722313464  ___inputMoveDirection_6;
	// System.Boolean CharacterMotor::inputJump
	bool ___inputJump_7;
	// CharacterMotorMovement CharacterMotor::movement
	CharacterMotorMovement_t2204346181 * ___movement_8;
	// CharacterMotorJumping CharacterMotor::jumping
	CharacterMotorJumping_t2963212952 * ___jumping_9;
	// CharacterMotorMovingPlatform CharacterMotor::movingPlatform
	CharacterMotorMovingPlatform_t3582163828 * ___movingPlatform_10;
	// CharacterMotorSliding CharacterMotor::sliding
	CharacterMotorSliding_t602719019 * ___sliding_11;
	// System.Boolean CharacterMotor::grounded
	bool ___grounded_12;
	// UnityEngine.Vector3 CharacterMotor::groundNormal
	Vector3_t3722313464  ___groundNormal_13;
	// UnityEngine.Vector3 CharacterMotor::lastGroundNormal
	Vector3_t3722313464  ___lastGroundNormal_14;
	// UnityEngine.Transform CharacterMotor::tr
	Transform_t3600365921 * ___tr_15;
	// UnityEngine.CharacterController CharacterMotor::controller
	CharacterController_t1138636865 * ___controller_16;

public:
	inline static int32_t get_offset_of_canControl_4() { return static_cast<int32_t>(offsetof(CharacterMotor_t1911343696, ___canControl_4)); }
	inline bool get_canControl_4() const { return ___canControl_4; }
	inline bool* get_address_of_canControl_4() { return &___canControl_4; }
	inline void set_canControl_4(bool value)
	{
		___canControl_4 = value;
	}

	inline static int32_t get_offset_of_useFixedUpdate_5() { return static_cast<int32_t>(offsetof(CharacterMotor_t1911343696, ___useFixedUpdate_5)); }
	inline bool get_useFixedUpdate_5() const { return ___useFixedUpdate_5; }
	inline bool* get_address_of_useFixedUpdate_5() { return &___useFixedUpdate_5; }
	inline void set_useFixedUpdate_5(bool value)
	{
		___useFixedUpdate_5 = value;
	}

	inline static int32_t get_offset_of_inputMoveDirection_6() { return static_cast<int32_t>(offsetof(CharacterMotor_t1911343696, ___inputMoveDirection_6)); }
	inline Vector3_t3722313464  get_inputMoveDirection_6() const { return ___inputMoveDirection_6; }
	inline Vector3_t3722313464 * get_address_of_inputMoveDirection_6() { return &___inputMoveDirection_6; }
	inline void set_inputMoveDirection_6(Vector3_t3722313464  value)
	{
		___inputMoveDirection_6 = value;
	}

	inline static int32_t get_offset_of_inputJump_7() { return static_cast<int32_t>(offsetof(CharacterMotor_t1911343696, ___inputJump_7)); }
	inline bool get_inputJump_7() const { return ___inputJump_7; }
	inline bool* get_address_of_inputJump_7() { return &___inputJump_7; }
	inline void set_inputJump_7(bool value)
	{
		___inputJump_7 = value;
	}

	inline static int32_t get_offset_of_movement_8() { return static_cast<int32_t>(offsetof(CharacterMotor_t1911343696, ___movement_8)); }
	inline CharacterMotorMovement_t2204346181 * get_movement_8() const { return ___movement_8; }
	inline CharacterMotorMovement_t2204346181 ** get_address_of_movement_8() { return &___movement_8; }
	inline void set_movement_8(CharacterMotorMovement_t2204346181 * value)
	{
		___movement_8 = value;
		Il2CppCodeGenWriteBarrier((&___movement_8), value);
	}

	inline static int32_t get_offset_of_jumping_9() { return static_cast<int32_t>(offsetof(CharacterMotor_t1911343696, ___jumping_9)); }
	inline CharacterMotorJumping_t2963212952 * get_jumping_9() const { return ___jumping_9; }
	inline CharacterMotorJumping_t2963212952 ** get_address_of_jumping_9() { return &___jumping_9; }
	inline void set_jumping_9(CharacterMotorJumping_t2963212952 * value)
	{
		___jumping_9 = value;
		Il2CppCodeGenWriteBarrier((&___jumping_9), value);
	}

	inline static int32_t get_offset_of_movingPlatform_10() { return static_cast<int32_t>(offsetof(CharacterMotor_t1911343696, ___movingPlatform_10)); }
	inline CharacterMotorMovingPlatform_t3582163828 * get_movingPlatform_10() const { return ___movingPlatform_10; }
	inline CharacterMotorMovingPlatform_t3582163828 ** get_address_of_movingPlatform_10() { return &___movingPlatform_10; }
	inline void set_movingPlatform_10(CharacterMotorMovingPlatform_t3582163828 * value)
	{
		___movingPlatform_10 = value;
		Il2CppCodeGenWriteBarrier((&___movingPlatform_10), value);
	}

	inline static int32_t get_offset_of_sliding_11() { return static_cast<int32_t>(offsetof(CharacterMotor_t1911343696, ___sliding_11)); }
	inline CharacterMotorSliding_t602719019 * get_sliding_11() const { return ___sliding_11; }
	inline CharacterMotorSliding_t602719019 ** get_address_of_sliding_11() { return &___sliding_11; }
	inline void set_sliding_11(CharacterMotorSliding_t602719019 * value)
	{
		___sliding_11 = value;
		Il2CppCodeGenWriteBarrier((&___sliding_11), value);
	}

	inline static int32_t get_offset_of_grounded_12() { return static_cast<int32_t>(offsetof(CharacterMotor_t1911343696, ___grounded_12)); }
	inline bool get_grounded_12() const { return ___grounded_12; }
	inline bool* get_address_of_grounded_12() { return &___grounded_12; }
	inline void set_grounded_12(bool value)
	{
		___grounded_12 = value;
	}

	inline static int32_t get_offset_of_groundNormal_13() { return static_cast<int32_t>(offsetof(CharacterMotor_t1911343696, ___groundNormal_13)); }
	inline Vector3_t3722313464  get_groundNormal_13() const { return ___groundNormal_13; }
	inline Vector3_t3722313464 * get_address_of_groundNormal_13() { return &___groundNormal_13; }
	inline void set_groundNormal_13(Vector3_t3722313464  value)
	{
		___groundNormal_13 = value;
	}

	inline static int32_t get_offset_of_lastGroundNormal_14() { return static_cast<int32_t>(offsetof(CharacterMotor_t1911343696, ___lastGroundNormal_14)); }
	inline Vector3_t3722313464  get_lastGroundNormal_14() const { return ___lastGroundNormal_14; }
	inline Vector3_t3722313464 * get_address_of_lastGroundNormal_14() { return &___lastGroundNormal_14; }
	inline void set_lastGroundNormal_14(Vector3_t3722313464  value)
	{
		___lastGroundNormal_14 = value;
	}

	inline static int32_t get_offset_of_tr_15() { return static_cast<int32_t>(offsetof(CharacterMotor_t1911343696, ___tr_15)); }
	inline Transform_t3600365921 * get_tr_15() const { return ___tr_15; }
	inline Transform_t3600365921 ** get_address_of_tr_15() { return &___tr_15; }
	inline void set_tr_15(Transform_t3600365921 * value)
	{
		___tr_15 = value;
		Il2CppCodeGenWriteBarrier((&___tr_15), value);
	}

	inline static int32_t get_offset_of_controller_16() { return static_cast<int32_t>(offsetof(CharacterMotor_t1911343696, ___controller_16)); }
	inline CharacterController_t1138636865 * get_controller_16() const { return ___controller_16; }
	inline CharacterController_t1138636865 ** get_address_of_controller_16() { return &___controller_16; }
	inline void set_controller_16(CharacterController_t1138636865 * value)
	{
		___controller_16 = value;
		Il2CppCodeGenWriteBarrier((&___controller_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERMOTOR_T1911343696_H
#ifndef FPSINPUTCONTROLLER_T1143113571_H
#define FPSINPUTCONTROLLER_T1143113571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FPSInputController
struct  FPSInputController_t1143113571  : public MonoBehaviour_t3962482529
{
public:
	// CharacterMotor FPSInputController::motor
	CharacterMotor_t1911343696 * ___motor_4;

public:
	inline static int32_t get_offset_of_motor_4() { return static_cast<int32_t>(offsetof(FPSInputController_t1143113571, ___motor_4)); }
	inline CharacterMotor_t1911343696 * get_motor_4() const { return ___motor_4; }
	inline CharacterMotor_t1911343696 ** get_address_of_motor_4() { return &___motor_4; }
	inline void set_motor_4(CharacterMotor_t1911343696 * value)
	{
		___motor_4 = value;
		Il2CppCodeGenWriteBarrier((&___motor_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSINPUTCONTROLLER_T1143113571_H
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1068524471  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Keyframe_t4206410242  m_Items[1];

public:
	inline Keyframe_t4206410242  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Keyframe_t4206410242 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Keyframe_t4206410242  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Keyframe_t4206410242  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Keyframe_t4206410242 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Keyframe_t4206410242  value)
	{
		m_Items[index] = value;
	}
};


// System.Void Boo.Lang.GenericGenerator`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void GenericGenerator_1__ctor_m599230948_gshared (GenericGenerator_1_t3943193262 * __this, const RuntimeMethod* method);
// System.Void Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void GenericGeneratorEnumerator_1__ctor_m496418942_gshared (GenericGeneratorEnumerator_1_t3544741914 * __this, const RuntimeMethod* method);
// System.Boolean Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::Yield(System.Int32,!0)
extern "C" IL2CPP_METHOD_ATTR bool GenericGeneratorEnumerator_1_Yield_m2645948398_gshared (GenericGeneratorEnumerator_1_t3544741914 * __this, int32_t p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Boolean Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::YieldDefault(System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool GenericGeneratorEnumerator_1_YieldDefault_m4013619751_gshared (GenericGeneratorEnumerator_1_t3544741914 * __this, int32_t p0, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_zero_m1409827619 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void CharacterMotorMovement::.ctor()
extern "C" IL2CPP_METHOD_ATTR void CharacterMotorMovement__ctor_m1785114476 (CharacterMotorMovement_t2204346181 * __this, const RuntimeMethod* method);
// System.Void CharacterMotorJumping::.ctor()
extern "C" IL2CPP_METHOD_ATTR void CharacterMotorJumping__ctor_m2578071375 (CharacterMotorJumping_t2963212952 * __this, const RuntimeMethod* method);
// System.Void CharacterMotorMovingPlatform::.ctor()
extern "C" IL2CPP_METHOD_ATTR void CharacterMotorMovingPlatform__ctor_m3446355503 (CharacterMotorMovingPlatform_t3582163828 * __this, const RuntimeMethod* method);
// System.Void CharacterMotorSliding::.ctor()
extern "C" IL2CPP_METHOD_ATTR void CharacterMotorSliding__ctor_m1828951945 (CharacterMotorSliding_t602719019 * __this, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C" IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m1620074514 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const RuntimeMethod* method);
// UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
extern "C" IL2CPP_METHOD_ATTR Component_t1923634451 * Component_GetComponent_m886226392 (Component_t1923634451 * __this, Type_t * p0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 CharacterMotor::ApplyInputVelocityChange(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  CharacterMotor_ApplyInputVelocityChange_m1754048112 (CharacterMotor_t1911343696 * __this, Vector3_t3722313464  ___velocity0, const RuntimeMethod* method);
// UnityEngine.Vector3 CharacterMotor::ApplyGravityAndJumping(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  CharacterMotor_ApplyGravityAndJumping_m3777966286 (CharacterMotor_t1911343696 * __this, Vector3_t3722313464  ___velocity0, const RuntimeMethod* method);
// System.Boolean CharacterMotor::MoveWithPlatform()
extern "C" IL2CPP_METHOD_ATTR bool CharacterMotor_MoveWithPlatform_m2230124238 (CharacterMotor_t1911343696 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_TransformPoint_m226827784 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_op_Subtraction_m3073674971 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector3::op_Inequality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR bool Vector3_op_Inequality_m315980366 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// UnityEngine.CollisionFlags UnityEngine.CharacterController::Move(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR int32_t CharacterController_Move_m1547317252 (CharacterController_t1138636865 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Transform_get_rotation_m3502953881 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Quaternion_op_Multiply_m1294064023 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  p0, Quaternion_t2301928331  p1, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Quaternion_Inverse_m1311579081 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Quaternion_get_eulerAngles_m3425202016 (Quaternion_t2301928331 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Transform_Rotate_m3172098886 (Transform_t3600365921 * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
extern "C" IL2CPP_METHOD_ATTR float Time_get_deltaTime_m372706562 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_op_Multiply_m3376773913 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const RuntimeMethod* method);
// System.Single UnityEngine.CharacterController::get_stepOffset()
extern "C" IL2CPP_METHOD_ATTR float CharacterController_get_stepOffset_m4263336387 (CharacterController_t1138636865 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector3__ctor_m3353183577 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_magnitude()
extern "C" IL2CPP_METHOD_ATTR float Vector3_get_magnitude_m27958459 (Vector3_t3722313464 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_Max_m3146388979 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_up_m3584168373 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(System.Single,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_op_Multiply_m2104357790 (RuntimeObject * __this /* static, unused */, float p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_localToWorldMatrix()
extern "C" IL2CPP_METHOD_ATTR Matrix4x4_t1817901843  Transform_get_localToWorldMatrix_m4155710351 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_op_Division_m510815599 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR bool Vector3_op_Equality_m4231250055 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR float Vector3_Dot_m606404487 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_sqrMagnitude()
extern "C" IL2CPP_METHOD_ATTR float Vector3_get_sqrMagnitude_m1474274574 (Vector3_t3722313464 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_Clamp01_m56433566 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_op_Addition_m779775034 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// System.Boolean CharacterMotor::IsGroundedTest()
extern "C" IL2CPP_METHOD_ATTR bool CharacterMotor_IsGroundedTest_m3364471484 (CharacterMotor_t1911343696 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Component::SendMessage(System.String,UnityEngine.SendMessageOptions)
extern "C" IL2CPP_METHOD_ATTR void Component_SendMessage_m1441147224 (Component_t1923634451 * __this, String_t* p0, int32_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_position_m3387557959 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// System.Collections.IEnumerator CharacterMotor::SubtractNewPlatformVelocity()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* CharacterMotor_SubtractNewPlatformVelocity_m2460956088 (CharacterMotor_t1911343696 * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C" IL2CPP_METHOD_ATTR Coroutine_t3829159415 * MonoBehaviour_StartCoroutine_m3411253000 (MonoBehaviour_t3962482529 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.CharacterController::get_center()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  CharacterController_get_center_m882281788 (CharacterController_t1138636865 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.CharacterController::get_height()
extern "C" IL2CPP_METHOD_ATTR float CharacterController_get_height_m4025328698 (CharacterController_t1138636865 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.CharacterController::get_radius()
extern "C" IL2CPP_METHOD_ATTR float CharacterController_get_radius_m4250137633 (CharacterController_t1138636865 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_InverseTransformPoint_m1343916000 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint3x4(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Matrix4x4_MultiplyPoint3x4_m4145063176 (Matrix4x4_t1817901843 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// System.Void CharacterMotor::UpdateFunction()
extern "C" IL2CPP_METHOD_ATTR void CharacterMotor_UpdateFunction_m3298730167 (CharacterMotor_t1911343696 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_normalized_m2454957984 (Vector3_t3722313464 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Project(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_Project_m899145139 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// UnityEngine.Vector3 CharacterMotor::GetDesiredHorizontalVelocity()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  CharacterMotor_GetDesiredHorizontalVelocity_m2565923170 (CharacterMotor_t1911343696 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 CharacterMotor::AdjustGroundVelocityToNormal(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  CharacterMotor_AdjustGroundVelocityToNormal_m1559675676 (CharacterMotor_t1911343696 * __this, Vector3_t3722313464  ___hVelocity0, Vector3_t3722313464  ___groundNormal1, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_Min_m1073399594 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_time()
extern "C" IL2CPP_METHOD_ATTR float Time_get_time_m2907476221 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Slerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_Slerp_m802114822 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, float p2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::get_normal()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  ControllerColliderHit_get_normal_m2699436127 (ControllerColliderHit_t240592346 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::get_moveDirection()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  ControllerColliderHit_get_moveDirection_m1770146420 (ControllerColliderHit_t240592346 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::get_point()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  ControllerColliderHit_get_point_m1769704767 (ControllerColliderHit_t240592346 * __this, const RuntimeMethod* method);
// UnityEngine.Collider UnityEngine.ControllerColliderHit::get_collider()
extern "C" IL2CPP_METHOD_ATTR Collider_t1773347010 * ControllerColliderHit_get_collider_m2639586580 (ControllerColliderHit_t240592346 * __this, const RuntimeMethod* method);
// System.Void CharacterMotor/$SubtractNewPlatformVelocity$1::.ctor(CharacterMotor)
extern "C" IL2CPP_METHOD_ATTR void U24SubtractNewPlatformVelocityU241__ctor_m2198776343 (U24SubtractNewPlatformVelocityU241_t3033866676 * __this, CharacterMotor_t1911343696 * ___self_0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<System.Object> CharacterMotor/$SubtractNewPlatformVelocity$1::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* U24SubtractNewPlatformVelocityU241_GetEnumerator_m3891665188 (U24SubtractNewPlatformVelocityU241_t3033866676 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformDirection(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_InverseTransformDirection_m3843238577 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
extern "C" IL2CPP_METHOD_ATTR float AnimationCurve_Evaluate_m2125563588 (AnimationCurve_t3046754366 * __this, float p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_TransformDirection_m3784028109 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Cross(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_Cross_m418170344 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// System.Single UnityEngine.CharacterController::get_slopeLimit()
extern "C" IL2CPP_METHOD_ATTR float CharacterController_get_slopeLimit_m485529875 (CharacterController_t1138636865 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Component::SendMessage(System.String)
extern "C" IL2CPP_METHOD_ATTR void Component_SendMessage_m3172125788 (Component_t1923634451 * __this, String_t* p0, const RuntimeMethod* method);
// System.Void Boo.Lang.GenericGenerator`1<System.Object>::.ctor()
inline void GenericGenerator_1__ctor_m599230948 (GenericGenerator_1_t3943193262 * __this, const RuntimeMethod* method)
{
	((  void (*) (GenericGenerator_1_t3943193262 *, const RuntimeMethod*))GenericGenerator_1__ctor_m599230948_gshared)(__this, method);
}
// System.Void CharacterMotor/$SubtractNewPlatformVelocity$1/$::.ctor(CharacterMotor)
extern "C" IL2CPP_METHOD_ATTR void U24__ctor_m4161687318 (U24_t1451678995 * __this, CharacterMotor_t1911343696 * ___self_0, const RuntimeMethod* method);
// System.Void Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::.ctor()
inline void GenericGeneratorEnumerator_1__ctor_m496418942 (GenericGeneratorEnumerator_1_t3544741914 * __this, const RuntimeMethod* method)
{
	((  void (*) (GenericGeneratorEnumerator_1_t3544741914 *, const RuntimeMethod*))GenericGeneratorEnumerator_1__ctor_m496418942_gshared)(__this, method);
}
// System.Void UnityEngine.WaitForFixedUpdate::.ctor()
extern "C" IL2CPP_METHOD_ATTR void WaitForFixedUpdate__ctor_m590323305 (WaitForFixedUpdate_t1634918743 * __this, const RuntimeMethod* method);
// System.Boolean Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::Yield(System.Int32,!0)
inline bool GenericGeneratorEnumerator_1_Yield_m2645948398 (GenericGeneratorEnumerator_1_t3544741914 * __this, int32_t p0, RuntimeObject * p1, const RuntimeMethod* method)
{
	return ((  bool (*) (GenericGeneratorEnumerator_1_t3544741914 *, int32_t, RuntimeObject *, const RuntimeMethod*))GenericGeneratorEnumerator_1_Yield_m2645948398_gshared)(__this, p0, p1, method);
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// System.Boolean Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::YieldDefault(System.Int32)
inline bool GenericGeneratorEnumerator_1_YieldDefault_m4013619751 (GenericGeneratorEnumerator_1_t3544741914 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  bool (*) (GenericGeneratorEnumerator_1_t3544741914 *, int32_t, const RuntimeMethod*))GenericGeneratorEnumerator_1_YieldDefault_m4013619751_gshared)(__this, p0, method);
}
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Keyframe::.ctor(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Keyframe__ctor_m391431887 (Keyframe_t4206410242 * __this, float p0, float p1, const RuntimeMethod* method);
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C" IL2CPP_METHOD_ATTR void AnimationCurve__ctor_m1565662948 (AnimationCurve_t3046754366 * __this, KeyframeU5BU5D_t1068524471* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Cursor::set_visible(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Cursor_set_visible_m2693238713 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method);
// System.Single UnityEngine.Input::GetAxis(System.String)
extern "C" IL2CPP_METHOD_ATTR float Input_GetAxis_m4009438427 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Quaternion_op_Multiply_m2607404835 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetButton(System.String)
extern "C" IL2CPP_METHOD_ATTR bool Input_GetButton_m2064261504 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CharacterMotor::.ctor()
extern "C" IL2CPP_METHOD_ATTR void CharacterMotor__ctor_m2676047606 (CharacterMotor_t1911343696 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor__ctor_m2676047606_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// #pragma strict
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		// var canControl : boolean = true;
		__this->set_canControl_4((bool)1);
		// var useFixedUpdate : boolean = true;
		__this->set_useFixedUpdate_5((bool)1);
		// var inputMoveDirection : Vector3 = Vector3.zero;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_0 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_inputMoveDirection_6(L_0);
		// var movement : CharacterMotorMovement = CharacterMotorMovement();
		CharacterMotorMovement_t2204346181 * L_1 = (CharacterMotorMovement_t2204346181 *)il2cpp_codegen_object_new(CharacterMotorMovement_t2204346181_il2cpp_TypeInfo_var);
		CharacterMotorMovement__ctor_m1785114476(L_1, /*hidden argument*/NULL);
		__this->set_movement_8(L_1);
		// var jumping : CharacterMotorJumping = CharacterMotorJumping();
		CharacterMotorJumping_t2963212952 * L_2 = (CharacterMotorJumping_t2963212952 *)il2cpp_codegen_object_new(CharacterMotorJumping_t2963212952_il2cpp_TypeInfo_var);
		CharacterMotorJumping__ctor_m2578071375(L_2, /*hidden argument*/NULL);
		__this->set_jumping_9(L_2);
		// var movingPlatform : CharacterMotorMovingPlatform = CharacterMotorMovingPlatform();
		CharacterMotorMovingPlatform_t3582163828 * L_3 = (CharacterMotorMovingPlatform_t3582163828 *)il2cpp_codegen_object_new(CharacterMotorMovingPlatform_t3582163828_il2cpp_TypeInfo_var);
		CharacterMotorMovingPlatform__ctor_m3446355503(L_3, /*hidden argument*/NULL);
		__this->set_movingPlatform_10(L_3);
		// var sliding : CharacterMotorSliding = CharacterMotorSliding();
		CharacterMotorSliding_t602719019 * L_4 = (CharacterMotorSliding_t602719019 *)il2cpp_codegen_object_new(CharacterMotorSliding_t602719019_il2cpp_TypeInfo_var);
		CharacterMotorSliding__ctor_m1828951945(L_4, /*hidden argument*/NULL);
		__this->set_sliding_11(L_4);
		// var grounded : boolean = true;
		__this->set_grounded_12((bool)1);
		// var groundNormal : Vector3 = Vector3.zero;
		Vector3_t3722313464  L_5 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_groundNormal_13(L_5);
		// private var lastGroundNormal : Vector3 = Vector3.zero;
		Vector3_t3722313464  L_6 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lastGroundNormal_14(L_6);
		return;
	}
}
// System.Void CharacterMotor::Awake()
extern "C" IL2CPP_METHOD_ATTR void CharacterMotor_Awake_m2777289438 (CharacterMotor_t1911343696 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_Awake_m2777289438_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// controller = GetComponent (CharacterController);
		RuntimeTypeHandle_t3027515415  L_0 = { reinterpret_cast<intptr_t> (CharacterController_t1138636865_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Component_t1923634451 * L_2 = Component_GetComponent_m886226392(__this, L_1, /*hidden argument*/NULL);
		__this->set_controller_16(((CharacterController_t1138636865 *)CastclassClass((RuntimeObject*)L_2, CharacterController_t1138636865_il2cpp_TypeInfo_var)));
		// tr = transform;
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		__this->set_tr_15(L_3);
		return;
	}
}
// System.Void CharacterMotor::UpdateFunction()
extern "C" IL2CPP_METHOD_ATTR void CharacterMotor_UpdateFunction_m3298730167 (CharacterMotor_t1911343696 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_UpdateFunction_m3298730167_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Quaternion_t2301928331  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Quaternion_t2301928331  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	Vector3_t3722313464  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t3722313464  V_7;
	memset(&V_7, 0, sizeof(V_7));
	float V_8 = 0.0f;
	Vector3_t3722313464  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t3722313464  V_10;
	memset(&V_10, 0, sizeof(V_10));
	float V_11 = 0.0f;
	Vector3_t3722313464  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t3722313464  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Vector3_t3722313464  V_14;
	memset(&V_14, 0, sizeof(V_14));
	{
		// var velocity : Vector3 = movement.velocity;
		CharacterMotorMovement_t2204346181 * L_0 = __this->get_movement_8();
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = L_0->get_velocity_9();
		V_0 = L_1;
		// velocity = ApplyInputVelocityChange(velocity);
		Vector3_t3722313464  L_2 = V_0;
		Vector3_t3722313464  L_3 = CharacterMotor_ApplyInputVelocityChange_m1754048112(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// velocity = ApplyGravityAndJumping (velocity);
		Vector3_t3722313464  L_4 = V_0;
		Vector3_t3722313464  L_5 = CharacterMotor_ApplyGravityAndJumping_m3777966286(__this, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		// var moveDistance : Vector3 = Vector3.zero;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_6 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_6;
		// if (MoveWithPlatform()) {
		bool L_7 = CharacterMotor_MoveWithPlatform_m2230124238(__this, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00dd;
		}
	}
	{
		// var newGlobalPoint : Vector3 = movingPlatform.activePlatform.TransformPoint(movingPlatform.activeLocalPoint);
		CharacterMotorMovingPlatform_t3582163828 * L_8 = __this->get_movingPlatform_10();
		NullCheck(L_8);
		Transform_t3600365921 * L_9 = L_8->get_activePlatform_3();
		CharacterMotorMovingPlatform_t3582163828 * L_10 = __this->get_movingPlatform_10();
		NullCheck(L_10);
		Vector3_t3722313464  L_11 = L_10->get_activeLocalPoint_4();
		NullCheck(L_9);
		Vector3_t3722313464  L_12 = Transform_TransformPoint_m226827784(L_9, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		// moveDistance = (newGlobalPoint - movingPlatform.activeGlobalPoint);
		Vector3_t3722313464  L_13 = V_2;
		CharacterMotorMovingPlatform_t3582163828 * L_14 = __this->get_movingPlatform_10();
		NullCheck(L_14);
		Vector3_t3722313464  L_15 = L_14->get_activeGlobalPoint_5();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_16 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		// if (moveDistance != Vector3.zero)
		Vector3_t3722313464  L_17 = V_1;
		Vector3_t3722313464  L_18 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_19 = Vector3_op_Inequality_m315980366(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0078;
		}
	}
	{
		// controller.Move(moveDistance);
		CharacterController_t1138636865 * L_20 = __this->get_controller_16();
		Vector3_t3722313464  L_21 = V_1;
		NullCheck(L_20);
		CharacterController_Move_m1547317252(L_20, L_21, /*hidden argument*/NULL);
	}

IL_0078:
	{
		// var newGlobalRotation : Quaternion = movingPlatform.activePlatform.rotation * movingPlatform.activeLocalRotation;
		CharacterMotorMovingPlatform_t3582163828 * L_22 = __this->get_movingPlatform_10();
		NullCheck(L_22);
		Transform_t3600365921 * L_23 = L_22->get_activePlatform_3();
		NullCheck(L_23);
		Quaternion_t2301928331  L_24 = Transform_get_rotation_m3502953881(L_23, /*hidden argument*/NULL);
		CharacterMotorMovingPlatform_t3582163828 * L_25 = __this->get_movingPlatform_10();
		NullCheck(L_25);
		Quaternion_t2301928331  L_26 = L_25->get_activeLocalRotation_6();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_27 = Quaternion_op_Multiply_m1294064023(NULL /*static, unused*/, L_24, L_26, /*hidden argument*/NULL);
		V_3 = L_27;
		// var rotationDiff : Quaternion = newGlobalRotation * Quaternion.Inverse(movingPlatform.activeGlobalRotation);
		Quaternion_t2301928331  L_28 = V_3;
		CharacterMotorMovingPlatform_t3582163828 * L_29 = __this->get_movingPlatform_10();
		NullCheck(L_29);
		Quaternion_t2301928331  L_30 = L_29->get_activeGlobalRotation_7();
		Quaternion_t2301928331  L_31 = Quaternion_Inverse_m1311579081(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_32 = Quaternion_op_Multiply_m1294064023(NULL /*static, unused*/, L_28, L_31, /*hidden argument*/NULL);
		V_4 = L_32;
		// var yRotation = rotationDiff.eulerAngles.y;
		Vector3_t3722313464  L_33 = Quaternion_get_eulerAngles_m3425202016((Quaternion_t2301928331 *)(&V_4), /*hidden argument*/NULL);
		V_12 = L_33;
		float L_34 = (&V_12)->get_y_3();
		V_5 = L_34;
		// if (yRotation != 0) {
		float L_35 = V_5;
		if ((((float)L_35) == ((float)(((float)((float)0))))))
		{
			goto IL_00dd;
		}
	}
	{
		// tr.Rotate(0, yRotation, 0);
		Transform_t3600365921 * L_36 = __this->get_tr_15();
		float L_37 = V_5;
		NullCheck(L_36);
		Transform_Rotate_m3172098886(L_36, (((float)((float)0))), L_37, (((float)((float)0))), /*hidden argument*/NULL);
	}

IL_00dd:
	{
		// var lastPosition : Vector3 = tr.position;
		Transform_t3600365921 * L_38 = __this->get_tr_15();
		NullCheck(L_38);
		Vector3_t3722313464  L_39 = Transform_get_position_m36019626(L_38, /*hidden argument*/NULL);
		V_6 = L_39;
		// var currentMovementOffset : Vector3 = velocity * Time.deltaTime;
		Vector3_t3722313464  L_40 = V_0;
		float L_41 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_42 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_40, L_41, /*hidden argument*/NULL);
		V_7 = L_42;
		// var pushDownOffset : float = Mathf.Max(controller.stepOffset, Vector3(currentMovementOffset.x, 0, currentMovementOffset.z).magnitude);
		CharacterController_t1138636865 * L_43 = __this->get_controller_16();
		NullCheck(L_43);
		float L_44 = CharacterController_get_stepOffset_m4263336387(L_43, /*hidden argument*/NULL);
		float L_45 = (&V_7)->get_x_2();
		float L_46 = (&V_7)->get_z_4();
		Vector3_t3722313464  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Vector3__ctor_m3353183577((&L_47), L_45, (((float)((float)0))), L_46, /*hidden argument*/NULL);
		V_13 = L_47;
		float L_48 = Vector3_get_magnitude_m27958459((Vector3_t3722313464 *)(&V_13), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_49 = Mathf_Max_m3146388979(NULL /*static, unused*/, L_44, L_48, /*hidden argument*/NULL);
		V_8 = L_49;
		// if (grounded)
		bool L_50 = __this->get_grounded_12();
		if (!L_50)
		{
			goto IL_0147;
		}
	}
	{
		// currentMovementOffset -= pushDownOffset * Vector3.up;
		Vector3_t3722313464  L_51 = V_7;
		float L_52 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_53 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_54 = Vector3_op_Multiply_m2104357790(NULL /*static, unused*/, L_52, L_53, /*hidden argument*/NULL);
		Vector3_t3722313464  L_55 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_51, L_54, /*hidden argument*/NULL);
		V_7 = L_55;
	}

IL_0147:
	{
		// movingPlatform.hitPlatform = null;
		CharacterMotorMovingPlatform_t3582163828 * L_56 = __this->get_movingPlatform_10();
		NullCheck(L_56);
		L_56->set_hitPlatform_2((Transform_t3600365921 *)NULL);
		// groundNormal = Vector3.zero;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_57 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_groundNormal_13(L_57);
		// movement.collisionFlags = controller.Move (currentMovementOffset);
		CharacterMotorMovement_t2204346181 * L_58 = __this->get_movement_8();
		CharacterController_t1138636865 * L_59 = __this->get_controller_16();
		Vector3_t3722313464  L_60 = V_7;
		NullCheck(L_59);
		int32_t L_61 = CharacterController_Move_m1547317252(L_59, L_60, /*hidden argument*/NULL);
		NullCheck(L_58);
		L_58->set_collisionFlags_8(L_61);
		// movement.lastHitPoint = movement.hitPoint;
		CharacterMotorMovement_t2204346181 * L_62 = __this->get_movement_8();
		CharacterMotorMovement_t2204346181 * L_63 = __this->get_movement_8();
		NullCheck(L_63);
		Vector3_t3722313464  L_64 = L_63->get_hitPoint_11();
		NullCheck(L_62);
		L_62->set_lastHitPoint_12(L_64);
		// lastGroundNormal = groundNormal;
		Vector3_t3722313464  L_65 = __this->get_groundNormal_13();
		__this->set_lastGroundNormal_14(L_65);
		// if (movingPlatform.enabled && movingPlatform.activePlatform != movingPlatform.hitPlatform) {
		CharacterMotorMovingPlatform_t3582163828 * L_66 = __this->get_movingPlatform_10();
		NullCheck(L_66);
		bool L_67 = L_66->get_enabled_0();
		if (!L_67)
		{
			goto IL_021b;
		}
	}
	{
		CharacterMotorMovingPlatform_t3582163828 * L_68 = __this->get_movingPlatform_10();
		NullCheck(L_68);
		Transform_t3600365921 * L_69 = L_68->get_activePlatform_3();
		CharacterMotorMovingPlatform_t3582163828 * L_70 = __this->get_movingPlatform_10();
		NullCheck(L_70);
		Transform_t3600365921 * L_71 = L_70->get_hitPlatform_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_72 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_69, L_71, /*hidden argument*/NULL);
		if (!L_72)
		{
			goto IL_021b;
		}
	}
	{
		// if (movingPlatform.hitPlatform != null) {
		CharacterMotorMovingPlatform_t3582163828 * L_73 = __this->get_movingPlatform_10();
		NullCheck(L_73);
		Transform_t3600365921 * L_74 = L_73->get_hitPlatform_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_75 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_74, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_75)
		{
			goto IL_021b;
		}
	}
	{
		// movingPlatform.activePlatform = movingPlatform.hitPlatform;
		CharacterMotorMovingPlatform_t3582163828 * L_76 = __this->get_movingPlatform_10();
		CharacterMotorMovingPlatform_t3582163828 * L_77 = __this->get_movingPlatform_10();
		NullCheck(L_77);
		Transform_t3600365921 * L_78 = L_77->get_hitPlatform_2();
		NullCheck(L_76);
		L_76->set_activePlatform_3(L_78);
		// movingPlatform.lastMatrix = movingPlatform.hitPlatform.localToWorldMatrix;
		CharacterMotorMovingPlatform_t3582163828 * L_79 = __this->get_movingPlatform_10();
		CharacterMotorMovingPlatform_t3582163828 * L_80 = __this->get_movingPlatform_10();
		NullCheck(L_80);
		Transform_t3600365921 * L_81 = L_80->get_hitPlatform_2();
		NullCheck(L_81);
		Matrix4x4_t1817901843  L_82 = Transform_get_localToWorldMatrix_m4155710351(L_81, /*hidden argument*/NULL);
		NullCheck(L_79);
		L_79->set_lastMatrix_8(L_82);
		// movingPlatform.newPlatform = true;
		CharacterMotorMovingPlatform_t3582163828 * L_83 = __this->get_movingPlatform_10();
		NullCheck(L_83);
		L_83->set_newPlatform_10((bool)1);
	}

IL_021b:
	{
		// var oldHVelocity : Vector3 = new Vector3(velocity.x, 0, velocity.z);
		float L_84 = (&V_0)->get_x_2();
		float L_85 = (&V_0)->get_z_4();
		Vector3_t3722313464  L_86;
		memset(&L_86, 0, sizeof(L_86));
		Vector3__ctor_m3353183577((&L_86), L_84, (((float)((float)0))), L_85, /*hidden argument*/NULL);
		V_9 = L_86;
		// movement.velocity = (tr.position - lastPosition) / Time.deltaTime;
		CharacterMotorMovement_t2204346181 * L_87 = __this->get_movement_8();
		Transform_t3600365921 * L_88 = __this->get_tr_15();
		NullCheck(L_88);
		Vector3_t3722313464  L_89 = Transform_get_position_m36019626(L_88, /*hidden argument*/NULL);
		Vector3_t3722313464  L_90 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_91 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_89, L_90, /*hidden argument*/NULL);
		float L_92 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_93 = Vector3_op_Division_m510815599(NULL /*static, unused*/, L_91, L_92, /*hidden argument*/NULL);
		NullCheck(L_87);
		L_87->set_velocity_9(L_93);
		// var newHVelocity : Vector3 = new Vector3(movement.velocity.x, 0, movement.velocity.z);
		CharacterMotorMovement_t2204346181 * L_94 = __this->get_movement_8();
		NullCheck(L_94);
		Vector3_t3722313464 * L_95 = L_94->get_address_of_velocity_9();
		float L_96 = L_95->get_x_2();
		CharacterMotorMovement_t2204346181 * L_97 = __this->get_movement_8();
		NullCheck(L_97);
		Vector3_t3722313464 * L_98 = L_97->get_address_of_velocity_9();
		float L_99 = L_98->get_z_4();
		Vector3_t3722313464  L_100;
		memset(&L_100, 0, sizeof(L_100));
		Vector3__ctor_m3353183577((&L_100), L_96, (((float)((float)0))), L_99, /*hidden argument*/NULL);
		V_10 = L_100;
		// if (oldHVelocity == Vector3.zero) {
		Vector3_t3722313464  L_101 = V_9;
		Vector3_t3722313464  L_102 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_103 = Vector3_op_Equality_m4231250055(NULL /*static, unused*/, L_101, L_102, /*hidden argument*/NULL);
		if (!L_103)
		{
			goto IL_02bc;
		}
	}
	{
		// movement.velocity = new Vector3(0, movement.velocity.y, 0);
		CharacterMotorMovement_t2204346181 * L_104 = __this->get_movement_8();
		CharacterMotorMovement_t2204346181 * L_105 = __this->get_movement_8();
		NullCheck(L_105);
		Vector3_t3722313464 * L_106 = L_105->get_address_of_velocity_9();
		float L_107 = L_106->get_y_3();
		Vector3_t3722313464  L_108;
		memset(&L_108, 0, sizeof(L_108));
		Vector3__ctor_m3353183577((&L_108), (((float)((float)0))), L_107, (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_104);
		L_104->set_velocity_9(L_108);
		goto IL_0307;
	}

IL_02bc:
	{
		// var projectedNewVelocity : float = Vector3.Dot(newHVelocity, oldHVelocity) / oldHVelocity.sqrMagnitude;
		Vector3_t3722313464  L_109 = V_10;
		Vector3_t3722313464  L_110 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_111 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_109, L_110, /*hidden argument*/NULL);
		float L_112 = Vector3_get_sqrMagnitude_m1474274574((Vector3_t3722313464 *)(&V_9), /*hidden argument*/NULL);
		V_11 = ((float)((float)L_111/(float)L_112));
		// movement.velocity = oldHVelocity * Mathf.Clamp01(projectedNewVelocity) + movement.velocity.y * Vector3.up;
		CharacterMotorMovement_t2204346181 * L_113 = __this->get_movement_8();
		Vector3_t3722313464  L_114 = V_9;
		float L_115 = V_11;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_116 = Mathf_Clamp01_m56433566(NULL /*static, unused*/, L_115, /*hidden argument*/NULL);
		Vector3_t3722313464  L_117 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_114, L_116, /*hidden argument*/NULL);
		CharacterMotorMovement_t2204346181 * L_118 = __this->get_movement_8();
		NullCheck(L_118);
		Vector3_t3722313464 * L_119 = L_118->get_address_of_velocity_9();
		float L_120 = L_119->get_y_3();
		Vector3_t3722313464  L_121 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_122 = Vector3_op_Multiply_m2104357790(NULL /*static, unused*/, L_120, L_121, /*hidden argument*/NULL);
		Vector3_t3722313464  L_123 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_117, L_122, /*hidden argument*/NULL);
		NullCheck(L_113);
		L_113->set_velocity_9(L_123);
	}

IL_0307:
	{
		// if (movement.velocity.y < velocity.y - 0.001) {
		CharacterMotorMovement_t2204346181 * L_124 = __this->get_movement_8();
		NullCheck(L_124);
		Vector3_t3722313464 * L_125 = L_124->get_address_of_velocity_9();
		float L_126 = L_125->get_y_3();
		float L_127 = (&V_0)->get_y_3();
		if ((((float)L_126) >= ((float)((float)il2cpp_codegen_subtract((float)L_127, (float)(0.001f))))))
		{
			goto IL_0368;
		}
	}
	{
		// if (movement.velocity.y < 0) {
		CharacterMotorMovement_t2204346181 * L_128 = __this->get_movement_8();
		NullCheck(L_128);
		Vector3_t3722313464 * L_129 = L_128->get_address_of_velocity_9();
		float L_130 = L_129->get_y_3();
		if ((((float)L_130) >= ((float)(((float)((float)0))))))
		{
			goto IL_035c;
		}
	}
	{
		// movement.velocity.y = velocity.y;
		CharacterMotorMovement_t2204346181 * L_131 = __this->get_movement_8();
		NullCheck(L_131);
		Vector3_t3722313464 * L_132 = L_131->get_address_of_velocity_9();
		float L_133 = (&V_0)->get_y_3();
		L_132->set_y_3(L_133);
		goto IL_0368;
	}

IL_035c:
	{
		// jumping.holdingJumpButton = false;
		CharacterMotorJumping_t2963212952 * L_134 = __this->get_jumping_9();
		NullCheck(L_134);
		L_134->set_holdingJumpButton_6((bool)0);
	}

IL_0368:
	{
		// if (grounded && !IsGroundedTest()) {
		bool L_135 = __this->get_grounded_12();
		if (!L_135)
		{
			goto IL_042b;
		}
	}
	{
		bool L_136 = CharacterMotor_IsGroundedTest_m3364471484(__this, /*hidden argument*/NULL);
		if (L_136)
		{
			goto IL_042b;
		}
	}
	{
		// grounded = false;
		__this->set_grounded_12((bool)0);
		// if (movingPlatform.enabled &&
		CharacterMotorMovingPlatform_t3582163828 * L_137 = __this->get_movingPlatform_10();
		NullCheck(L_137);
		bool L_138 = L_137->get_enabled_0();
		if (!L_138)
		{
			goto IL_03f3;
		}
	}
	{
		CharacterMotorMovingPlatform_t3582163828 * L_139 = __this->get_movingPlatform_10();
		NullCheck(L_139);
		int32_t L_140 = L_139->get_movementTransfer_1();
		if ((((int32_t)L_140) == ((int32_t)1)))
		{
			goto IL_03b7;
		}
	}
	{
		CharacterMotorMovingPlatform_t3582163828 * L_141 = __this->get_movingPlatform_10();
		NullCheck(L_141);
		int32_t L_142 = L_141->get_movementTransfer_1();
		if ((!(((uint32_t)L_142) == ((uint32_t)2))))
		{
			goto IL_03f3;
		}
	}

IL_03b7:
	{
		// movement.frameVelocity = movingPlatform.platformVelocity;
		CharacterMotorMovement_t2204346181 * L_143 = __this->get_movement_8();
		CharacterMotorMovingPlatform_t3582163828 * L_144 = __this->get_movingPlatform_10();
		NullCheck(L_144);
		Vector3_t3722313464  L_145 = L_144->get_platformVelocity_9();
		NullCheck(L_143);
		L_143->set_frameVelocity_10(L_145);
		// movement.velocity += movingPlatform.platformVelocity;
		CharacterMotorMovement_t2204346181 * L_146 = __this->get_movement_8();
		CharacterMotorMovement_t2204346181 * L_147 = __this->get_movement_8();
		NullCheck(L_147);
		Vector3_t3722313464  L_148 = L_147->get_velocity_9();
		CharacterMotorMovingPlatform_t3582163828 * L_149 = __this->get_movingPlatform_10();
		NullCheck(L_149);
		Vector3_t3722313464  L_150 = L_149->get_platformVelocity_9();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_151 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_148, L_150, /*hidden argument*/NULL);
		NullCheck(L_146);
		L_146->set_velocity_9(L_151);
	}

IL_03f3:
	{
		// SendMessage("OnFall", SendMessageOptions.DontRequireReceiver);
		Component_SendMessage_m1441147224(__this, _stringLiteral1307271031, 1, /*hidden argument*/NULL);
		// tr.position += pushDownOffset * Vector3.up;
		Transform_t3600365921 * L_152 = __this->get_tr_15();
		Transform_t3600365921 * L_153 = __this->get_tr_15();
		NullCheck(L_153);
		Vector3_t3722313464  L_154 = Transform_get_position_m36019626(L_153, /*hidden argument*/NULL);
		float L_155 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_156 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_157 = Vector3_op_Multiply_m2104357790(NULL /*static, unused*/, L_155, L_156, /*hidden argument*/NULL);
		Vector3_t3722313464  L_158 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_154, L_157, /*hidden argument*/NULL);
		NullCheck(L_152);
		Transform_set_position_m3387557959(L_152, L_158, /*hidden argument*/NULL);
		goto IL_046d;
	}

IL_042b:
	{
		// else if (!grounded && IsGroundedTest()) {
		bool L_159 = __this->get_grounded_12();
		if (L_159)
		{
			goto IL_046d;
		}
	}
	{
		bool L_160 = CharacterMotor_IsGroundedTest_m3364471484(__this, /*hidden argument*/NULL);
		if (!L_160)
		{
			goto IL_046d;
		}
	}
	{
		// grounded = true;
		__this->set_grounded_12((bool)1);
		// jumping.jumping = false;
		CharacterMotorJumping_t2963212952 * L_161 = __this->get_jumping_9();
		NullCheck(L_161);
		L_161->set_jumping_5((bool)0);
		// SubtractNewPlatformVelocity();
		RuntimeObject* L_162 = CharacterMotor_SubtractNewPlatformVelocity_m2460956088(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_162, /*hidden argument*/NULL);
		// SendMessage("OnLand", SendMessageOptions.DontRequireReceiver);
		Component_SendMessage_m1441147224(__this, _stringLiteral4082684263, 1, /*hidden argument*/NULL);
	}

IL_046d:
	{
		// if (MoveWithPlatform()) {
		bool L_163 = CharacterMotor_MoveWithPlatform_m2230124238(__this, /*hidden argument*/NULL);
		if (!L_163)
		{
			goto IL_053b;
		}
	}
	{
		// movingPlatform.activeGlobalPoint = tr.position + Vector3.up * (controller.center.y - controller.height*0.5 + controller.radius);
		CharacterMotorMovingPlatform_t3582163828 * L_164 = __this->get_movingPlatform_10();
		Transform_t3600365921 * L_165 = __this->get_tr_15();
		NullCheck(L_165);
		Vector3_t3722313464  L_166 = Transform_get_position_m36019626(L_165, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_167 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		CharacterController_t1138636865 * L_168 = __this->get_controller_16();
		NullCheck(L_168);
		Vector3_t3722313464  L_169 = CharacterController_get_center_m882281788(L_168, /*hidden argument*/NULL);
		V_14 = L_169;
		float L_170 = (&V_14)->get_y_3();
		CharacterController_t1138636865 * L_171 = __this->get_controller_16();
		NullCheck(L_171);
		float L_172 = CharacterController_get_height_m4025328698(L_171, /*hidden argument*/NULL);
		CharacterController_t1138636865 * L_173 = __this->get_controller_16();
		NullCheck(L_173);
		float L_174 = CharacterController_get_radius_m4250137633(L_173, /*hidden argument*/NULL);
		Vector3_t3722313464  L_175 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_167, ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_subtract((float)L_170, (float)((float)il2cpp_codegen_multiply((float)L_172, (float)(0.5f))))), (float)L_174)), /*hidden argument*/NULL);
		Vector3_t3722313464  L_176 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_166, L_175, /*hidden argument*/NULL);
		NullCheck(L_164);
		L_164->set_activeGlobalPoint_5(L_176);
		// movingPlatform.activeLocalPoint = movingPlatform.activePlatform.InverseTransformPoint(movingPlatform.activeGlobalPoint);
		CharacterMotorMovingPlatform_t3582163828 * L_177 = __this->get_movingPlatform_10();
		CharacterMotorMovingPlatform_t3582163828 * L_178 = __this->get_movingPlatform_10();
		NullCheck(L_178);
		Transform_t3600365921 * L_179 = L_178->get_activePlatform_3();
		CharacterMotorMovingPlatform_t3582163828 * L_180 = __this->get_movingPlatform_10();
		NullCheck(L_180);
		Vector3_t3722313464  L_181 = L_180->get_activeGlobalPoint_5();
		NullCheck(L_179);
		Vector3_t3722313464  L_182 = Transform_InverseTransformPoint_m1343916000(L_179, L_181, /*hidden argument*/NULL);
		NullCheck(L_177);
		L_177->set_activeLocalPoint_4(L_182);
		// movingPlatform.activeGlobalRotation = tr.rotation;
		CharacterMotorMovingPlatform_t3582163828 * L_183 = __this->get_movingPlatform_10();
		Transform_t3600365921 * L_184 = __this->get_tr_15();
		NullCheck(L_184);
		Quaternion_t2301928331  L_185 = Transform_get_rotation_m3502953881(L_184, /*hidden argument*/NULL);
		NullCheck(L_183);
		L_183->set_activeGlobalRotation_7(L_185);
		// movingPlatform.activeLocalRotation = Quaternion.Inverse(movingPlatform.activePlatform.rotation) * movingPlatform.activeGlobalRotation;
		CharacterMotorMovingPlatform_t3582163828 * L_186 = __this->get_movingPlatform_10();
		CharacterMotorMovingPlatform_t3582163828 * L_187 = __this->get_movingPlatform_10();
		NullCheck(L_187);
		Transform_t3600365921 * L_188 = L_187->get_activePlatform_3();
		NullCheck(L_188);
		Quaternion_t2301928331  L_189 = Transform_get_rotation_m3502953881(L_188, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_190 = Quaternion_Inverse_m1311579081(NULL /*static, unused*/, L_189, /*hidden argument*/NULL);
		CharacterMotorMovingPlatform_t3582163828 * L_191 = __this->get_movingPlatform_10();
		NullCheck(L_191);
		Quaternion_t2301928331  L_192 = L_191->get_activeGlobalRotation_7();
		Quaternion_t2301928331  L_193 = Quaternion_op_Multiply_m1294064023(NULL /*static, unused*/, L_190, L_192, /*hidden argument*/NULL);
		NullCheck(L_186);
		L_186->set_activeLocalRotation_6(L_193);
	}

IL_053b:
	{
		return;
	}
}
// System.Void CharacterMotor::FixedUpdate()
extern "C" IL2CPP_METHOD_ATTR void CharacterMotor_FixedUpdate_m4065519527 (CharacterMotor_t1911343696 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_FixedUpdate_m4065519527_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t1817901843  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		// if (movingPlatform.enabled) {
		CharacterMotorMovingPlatform_t3582163828 * L_0 = __this->get_movingPlatform_10();
		NullCheck(L_0);
		bool L_1 = L_0->get_enabled_0();
		if (!L_1)
		{
			goto IL_00d6;
		}
	}
	{
		// if (movingPlatform.activePlatform != null) {
		CharacterMotorMovingPlatform_t3582163828 * L_2 = __this->get_movingPlatform_10();
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = L_2->get_activePlatform_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_00c6;
		}
	}
	{
		// if (!movingPlatform.newPlatform) {
		CharacterMotorMovingPlatform_t3582163828 * L_5 = __this->get_movingPlatform_10();
		NullCheck(L_5);
		bool L_6 = L_5->get_newPlatform_10();
		if (L_6)
		{
			goto IL_009a;
		}
	}
	{
		// var lastVelocity : Vector3 = movingPlatform.platformVelocity;
		CharacterMotorMovingPlatform_t3582163828 * L_7 = __this->get_movingPlatform_10();
		NullCheck(L_7);
		Vector3_t3722313464  L_8 = L_7->get_platformVelocity_9();
		V_0 = L_8;
		// movingPlatform.platformVelocity = (
		CharacterMotorMovingPlatform_t3582163828 * L_9 = __this->get_movingPlatform_10();
		CharacterMotorMovingPlatform_t3582163828 * L_10 = __this->get_movingPlatform_10();
		NullCheck(L_10);
		Transform_t3600365921 * L_11 = L_10->get_activePlatform_3();
		NullCheck(L_11);
		Matrix4x4_t1817901843  L_12 = Transform_get_localToWorldMatrix_m4155710351(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		CharacterMotorMovingPlatform_t3582163828 * L_13 = __this->get_movingPlatform_10();
		NullCheck(L_13);
		Vector3_t3722313464  L_14 = L_13->get_activeLocalPoint_4();
		Vector3_t3722313464  L_15 = Matrix4x4_MultiplyPoint3x4_m4145063176((Matrix4x4_t1817901843 *)(&V_1), L_14, /*hidden argument*/NULL);
		CharacterMotorMovingPlatform_t3582163828 * L_16 = __this->get_movingPlatform_10();
		NullCheck(L_16);
		Matrix4x4_t1817901843 * L_17 = L_16->get_address_of_lastMatrix_8();
		CharacterMotorMovingPlatform_t3582163828 * L_18 = __this->get_movingPlatform_10();
		NullCheck(L_18);
		Vector3_t3722313464  L_19 = L_18->get_activeLocalPoint_4();
		Vector3_t3722313464  L_20 = Matrix4x4_MultiplyPoint3x4_m4145063176((Matrix4x4_t1817901843 *)L_17, L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_21 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_15, L_20, /*hidden argument*/NULL);
		float L_22 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_23 = Vector3_op_Division_m510815599(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_9);
		L_9->set_platformVelocity_9(L_23);
	}

IL_009a:
	{
		// movingPlatform.lastMatrix = movingPlatform.activePlatform.localToWorldMatrix;
		CharacterMotorMovingPlatform_t3582163828 * L_24 = __this->get_movingPlatform_10();
		CharacterMotorMovingPlatform_t3582163828 * L_25 = __this->get_movingPlatform_10();
		NullCheck(L_25);
		Transform_t3600365921 * L_26 = L_25->get_activePlatform_3();
		NullCheck(L_26);
		Matrix4x4_t1817901843  L_27 = Transform_get_localToWorldMatrix_m4155710351(L_26, /*hidden argument*/NULL);
		NullCheck(L_24);
		L_24->set_lastMatrix_8(L_27);
		// movingPlatform.newPlatform = false;
		CharacterMotorMovingPlatform_t3582163828 * L_28 = __this->get_movingPlatform_10();
		NullCheck(L_28);
		L_28->set_newPlatform_10((bool)0);
		goto IL_00d6;
	}

IL_00c6:
	{
		// movingPlatform.platformVelocity = Vector3.zero;
		CharacterMotorMovingPlatform_t3582163828 * L_29 = __this->get_movingPlatform_10();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_30 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_29);
		L_29->set_platformVelocity_9(L_30);
	}

IL_00d6:
	{
		// if (useFixedUpdate)
		bool L_31 = __this->get_useFixedUpdate_5();
		if (!L_31)
		{
			goto IL_00e7;
		}
	}
	{
		// UpdateFunction();
		CharacterMotor_UpdateFunction_m3298730167(__this, /*hidden argument*/NULL);
	}

IL_00e7:
	{
		return;
	}
}
// System.Void CharacterMotor::Update()
extern "C" IL2CPP_METHOD_ATTR void CharacterMotor_Update_m586127111 (CharacterMotor_t1911343696 * __this, const RuntimeMethod* method)
{
	{
		// if (!useFixedUpdate)
		bool L_0 = __this->get_useFixedUpdate_5();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		// UpdateFunction();
		CharacterMotor_UpdateFunction_m3298730167(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// UnityEngine.Vector3 CharacterMotor::ApplyInputVelocityChange(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  CharacterMotor_ApplyInputVelocityChange_m1754048112 (CharacterMotor_t1911343696 * __this, Vector3_t3722313464  ___velocity0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_ApplyInputVelocityChange_m1754048112_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		// if (!canControl)
		bool L_0 = __this->get_canControl_4();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		// inputMoveDirection = Vector3.zero;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_1 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_inputMoveDirection_6(L_1);
	}

IL_0016:
	{
		// var desiredVelocity : Vector3;
		il2cpp_codegen_initobj((&V_0), sizeof(Vector3_t3722313464 ));
		// if (grounded && TooSteep()) {
		bool L_2 = __this->get_grounded_12();
		if (!L_2)
		{
			goto IL_00b8;
		}
	}
	{
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(14 /* System.Boolean CharacterMotor::TooSteep() */, __this);
		if (!L_3)
		{
			goto IL_00b8;
		}
	}
	{
		// desiredVelocity = Vector3(groundNormal.x, 0, groundNormal.z).normalized;
		Vector3_t3722313464 * L_4 = __this->get_address_of_groundNormal_13();
		float L_5 = L_4->get_x_2();
		Vector3_t3722313464 * L_6 = __this->get_address_of_groundNormal_13();
		float L_7 = L_6->get_z_4();
		Vector3_t3722313464  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector3__ctor_m3353183577((&L_8), L_5, (((float)((float)0))), L_7, /*hidden argument*/NULL);
		V_4 = L_8;
		Vector3_t3722313464  L_9 = Vector3_get_normalized_m2454957984((Vector3_t3722313464 *)(&V_4), /*hidden argument*/NULL);
		V_0 = L_9;
		// var projectedMoveDir = Vector3.Project(inputMoveDirection, desiredVelocity);
		Vector3_t3722313464  L_10 = __this->get_inputMoveDirection_6();
		Vector3_t3722313464  L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_12 = Vector3_Project_m899145139(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		// desiredVelocity = desiredVelocity + projectedMoveDir * sliding.speedControl + (inputMoveDirection - projectedMoveDir) * sliding.sidewaysControl;
		Vector3_t3722313464  L_13 = V_0;
		Vector3_t3722313464  L_14 = V_1;
		CharacterMotorSliding_t602719019 * L_15 = __this->get_sliding_11();
		NullCheck(L_15);
		float L_16 = L_15->get_speedControl_3();
		Vector3_t3722313464  L_17 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_14, L_16, /*hidden argument*/NULL);
		Vector3_t3722313464  L_18 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_13, L_17, /*hidden argument*/NULL);
		Vector3_t3722313464  L_19 = __this->get_inputMoveDirection_6();
		Vector3_t3722313464  L_20 = V_1;
		Vector3_t3722313464  L_21 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		CharacterMotorSliding_t602719019 * L_22 = __this->get_sliding_11();
		NullCheck(L_22);
		float L_23 = L_22->get_sidewaysControl_2();
		Vector3_t3722313464  L_24 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_21, L_23, /*hidden argument*/NULL);
		Vector3_t3722313464  L_25 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_18, L_24, /*hidden argument*/NULL);
		V_0 = L_25;
		// desiredVelocity *= sliding.slidingSpeed;
		Vector3_t3722313464  L_26 = V_0;
		CharacterMotorSliding_t602719019 * L_27 = __this->get_sliding_11();
		NullCheck(L_27);
		float L_28 = L_27->get_slidingSpeed_1();
		Vector3_t3722313464  L_29 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_26, L_28, /*hidden argument*/NULL);
		V_0 = L_29;
		goto IL_00bf;
	}

IL_00b8:
	{
		// desiredVelocity = GetDesiredHorizontalVelocity();
		Vector3_t3722313464  L_30 = CharacterMotor_GetDesiredHorizontalVelocity_m2565923170(__this, /*hidden argument*/NULL);
		V_0 = L_30;
	}

IL_00bf:
	{
		// if (movingPlatform.enabled && movingPlatform.movementTransfer == MovementTransferOnJump.PermaTransfer) {
		CharacterMotorMovingPlatform_t3582163828 * L_31 = __this->get_movingPlatform_10();
		NullCheck(L_31);
		bool L_32 = L_31->get_enabled_0();
		if (!L_32)
		{
			goto IL_00fb;
		}
	}
	{
		CharacterMotorMovingPlatform_t3582163828 * L_33 = __this->get_movingPlatform_10();
		NullCheck(L_33);
		int32_t L_34 = L_33->get_movementTransfer_1();
		if ((!(((uint32_t)L_34) == ((uint32_t)2))))
		{
			goto IL_00fb;
		}
	}
	{
		// desiredVelocity += movement.frameVelocity;
		Vector3_t3722313464  L_35 = V_0;
		CharacterMotorMovement_t2204346181 * L_36 = __this->get_movement_8();
		NullCheck(L_36);
		Vector3_t3722313464  L_37 = L_36->get_frameVelocity_10();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_38 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_35, L_37, /*hidden argument*/NULL);
		V_0 = L_38;
		// desiredVelocity.y = 0;
		(&V_0)->set_y_3((((float)((float)0))));
	}

IL_00fb:
	{
		// if (grounded)
		bool L_39 = __this->get_grounded_12();
		if (!L_39)
		{
			goto IL_0119;
		}
	}
	{
		// desiredVelocity = AdjustGroundVelocityToNormal(desiredVelocity, groundNormal);
		Vector3_t3722313464  L_40 = V_0;
		Vector3_t3722313464  L_41 = __this->get_groundNormal_13();
		Vector3_t3722313464  L_42 = CharacterMotor_AdjustGroundVelocityToNormal_m1559675676(__this, L_40, L_41, /*hidden argument*/NULL);
		V_0 = L_42;
		goto IL_0126;
	}

IL_0119:
	{
		// velocity.y = 0;
		(&___velocity0)->set_y_3((((float)((float)0))));
	}

IL_0126:
	{
		// var maxVelocityChange : float = GetMaxAcceleration(grounded) * Time.deltaTime;
		bool L_43 = __this->get_grounded_12();
		float L_44 = VirtFuncInvoker1< float, bool >::Invoke(8 /* System.Single CharacterMotor::GetMaxAcceleration(System.Boolean) */, __this, L_43);
		float L_45 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = ((float)il2cpp_codegen_multiply((float)L_44, (float)L_45));
		// var velocityChangeVector : Vector3 = (desiredVelocity - velocity);
		Vector3_t3722313464  L_46 = V_0;
		Vector3_t3722313464  L_47 = ___velocity0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_48 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_46, L_47, /*hidden argument*/NULL);
		V_3 = L_48;
		// if (velocityChangeVector.sqrMagnitude > maxVelocityChange * maxVelocityChange) {
		float L_49 = Vector3_get_sqrMagnitude_m1474274574((Vector3_t3722313464 *)(&V_3), /*hidden argument*/NULL);
		float L_50 = V_2;
		float L_51 = V_2;
		if ((((float)L_49) <= ((float)((float)il2cpp_codegen_multiply((float)L_50, (float)L_51)))))
		{
			goto IL_015e;
		}
	}
	{
		// velocityChangeVector = velocityChangeVector.normalized * maxVelocityChange;
		Vector3_t3722313464  L_52 = Vector3_get_normalized_m2454957984((Vector3_t3722313464 *)(&V_3), /*hidden argument*/NULL);
		float L_53 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_54 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_52, L_53, /*hidden argument*/NULL);
		V_3 = L_54;
	}

IL_015e:
	{
		// if (grounded || canControl)
		bool L_55 = __this->get_grounded_12();
		if (L_55)
		{
			goto IL_0174;
		}
	}
	{
		bool L_56 = __this->get_canControl_4();
		if (!L_56)
		{
			goto IL_0181;
		}
	}

IL_0174:
	{
		// velocity += velocityChangeVector;
		Vector3_t3722313464  L_57 = ___velocity0;
		Vector3_t3722313464  L_58 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_59 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_57, L_58, /*hidden argument*/NULL);
		___velocity0 = L_59;
	}

IL_0181:
	{
		// if (grounded) {
		bool L_60 = __this->get_grounded_12();
		if (!L_60)
		{
			goto IL_01a9;
		}
	}
	{
		// velocity.y = Mathf.Min(velocity.y, 0);
		float L_61 = (&___velocity0)->get_y_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_62 = Mathf_Min_m1073399594(NULL /*static, unused*/, L_61, (((float)((float)0))), /*hidden argument*/NULL);
		(&___velocity0)->set_y_3(L_62);
	}

IL_01a9:
	{
		// return velocity;
		Vector3_t3722313464  L_63 = ___velocity0;
		return L_63;
	}
}
// UnityEngine.Vector3 CharacterMotor::ApplyGravityAndJumping(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  CharacterMotor_ApplyGravityAndJumping_m3777966286 (CharacterMotor_t1911343696 * __this, Vector3_t3722313464  ___velocity0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_ApplyGravityAndJumping_m3777966286_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!inputJump || !canControl) {
		bool L_0 = __this->get_inputJump_7();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = __this->get_canControl_4();
		if (L_1)
		{
			goto IL_0030;
		}
	}

IL_0016:
	{
		// jumping.holdingJumpButton = false;
		CharacterMotorJumping_t2963212952 * L_2 = __this->get_jumping_9();
		NullCheck(L_2);
		L_2->set_holdingJumpButton_6((bool)0);
		// jumping.lastButtonDownTime = -100;
		CharacterMotorJumping_t2963212952 * L_3 = __this->get_jumping_9();
		NullCheck(L_3);
		L_3->set_lastButtonDownTime_8((((float)((float)((int32_t)-100)))));
	}

IL_0030:
	{
		// if (inputJump && jumping.lastButtonDownTime < 0 && canControl)
		bool L_4 = __this->get_inputJump_7();
		if (!L_4)
		{
			goto IL_0068;
		}
	}
	{
		CharacterMotorJumping_t2963212952 * L_5 = __this->get_jumping_9();
		NullCheck(L_5);
		float L_6 = L_5->get_lastButtonDownTime_8();
		if ((((float)L_6) >= ((float)(((float)((float)0))))))
		{
			goto IL_0068;
		}
	}
	{
		bool L_7 = __this->get_canControl_4();
		if (!L_7)
		{
			goto IL_0068;
		}
	}
	{
		// jumping.lastButtonDownTime = Time.time;
		CharacterMotorJumping_t2963212952 * L_8 = __this->get_jumping_9();
		float L_9 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		L_8->set_lastButtonDownTime_8(L_9);
	}

IL_0068:
	{
		// if (grounded)
		bool L_10 = __this->get_grounded_12();
		if (!L_10)
		{
			goto IL_00a7;
		}
	}
	{
		// velocity.y = Mathf.Min(0, velocity.y) - movement.gravity * Time.deltaTime;
		float L_11 = (&___velocity0)->get_y_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Min_m1073399594(NULL /*static, unused*/, (((float)((float)0))), L_11, /*hidden argument*/NULL);
		CharacterMotorMovement_t2204346181 * L_13 = __this->get_movement_8();
		NullCheck(L_13);
		float L_14 = L_13->get_gravity_6();
		float L_15 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&___velocity0)->set_y_3(((float)il2cpp_codegen_subtract((float)L_12, (float)((float)il2cpp_codegen_multiply((float)L_14, (float)L_15)))));
		goto IL_017f;
	}

IL_00a7:
	{
		// velocity.y = movement.velocity.y - movement.gravity * Time.deltaTime;
		CharacterMotorMovement_t2204346181 * L_16 = __this->get_movement_8();
		NullCheck(L_16);
		Vector3_t3722313464 * L_17 = L_16->get_address_of_velocity_9();
		float L_18 = L_17->get_y_3();
		CharacterMotorMovement_t2204346181 * L_19 = __this->get_movement_8();
		NullCheck(L_19);
		float L_20 = L_19->get_gravity_6();
		float L_21 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&___velocity0)->set_y_3(((float)il2cpp_codegen_subtract((float)L_18, (float)((float)il2cpp_codegen_multiply((float)L_20, (float)L_21)))));
		// if (jumping.jumping && jumping.holdingJumpButton) {
		CharacterMotorJumping_t2963212952 * L_22 = __this->get_jumping_9();
		NullCheck(L_22);
		bool L_23 = L_22->get_jumping_5();
		if (!L_23)
		{
			goto IL_0158;
		}
	}
	{
		CharacterMotorJumping_t2963212952 * L_24 = __this->get_jumping_9();
		NullCheck(L_24);
		bool L_25 = L_24->get_holdingJumpButton_6();
		if (!L_25)
		{
			goto IL_0158;
		}
	}
	{
		// if (Time.time < jumping.lastStartTime + jumping.extraHeight / CalculateJumpVerticalSpeed(jumping.baseHeight)) {
		float L_26 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		CharacterMotorJumping_t2963212952 * L_27 = __this->get_jumping_9();
		NullCheck(L_27);
		float L_28 = L_27->get_lastStartTime_7();
		CharacterMotorJumping_t2963212952 * L_29 = __this->get_jumping_9();
		NullCheck(L_29);
		float L_30 = L_29->get_extraHeight_2();
		CharacterMotorJumping_t2963212952 * L_31 = __this->get_jumping_9();
		NullCheck(L_31);
		float L_32 = L_31->get_baseHeight_1();
		float L_33 = VirtFuncInvoker1< float, float >::Invoke(9 /* System.Single CharacterMotor::CalculateJumpVerticalSpeed(System.Single) */, __this, L_32);
		if ((((float)L_26) >= ((float)((float)il2cpp_codegen_add((float)L_28, (float)((float)((float)L_30/(float)L_33)))))))
		{
			goto IL_0158;
		}
	}
	{
		// velocity += jumping.jumpDir * movement.gravity * Time.deltaTime;
		Vector3_t3722313464  L_34 = ___velocity0;
		CharacterMotorJumping_t2963212952 * L_35 = __this->get_jumping_9();
		NullCheck(L_35);
		Vector3_t3722313464  L_36 = L_35->get_jumpDir_9();
		CharacterMotorMovement_t2204346181 * L_37 = __this->get_movement_8();
		NullCheck(L_37);
		float L_38 = L_37->get_gravity_6();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_39 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_36, L_38, /*hidden argument*/NULL);
		float L_40 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_41 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t3722313464  L_42 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_34, L_41, /*hidden argument*/NULL);
		___velocity0 = L_42;
	}

IL_0158:
	{
		// velocity.y = Mathf.Max (velocity.y, -movement.maxFallSpeed);
		float L_43 = (&___velocity0)->get_y_3();
		CharacterMotorMovement_t2204346181 * L_44 = __this->get_movement_8();
		NullCheck(L_44);
		float L_45 = L_44->get_maxFallSpeed_7();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_46 = Mathf_Max_m3146388979(NULL /*static, unused*/, L_43, ((-L_45)), /*hidden argument*/NULL);
		(&___velocity0)->set_y_3(L_46);
	}

IL_017f:
	{
		// if (grounded) {
		bool L_47 = __this->get_grounded_12();
		if (!L_47)
		{
			goto IL_030f;
		}
	}
	{
		// if (jumping.enabled && canControl && (Time.time - jumping.lastButtonDownTime < 0.2)) {
		CharacterMotorJumping_t2963212952 * L_48 = __this->get_jumping_9();
		NullCheck(L_48);
		bool L_49 = L_48->get_enabled_0();
		if (!L_49)
		{
			goto IL_0303;
		}
	}
	{
		bool L_50 = __this->get_canControl_4();
		if (!L_50)
		{
			goto IL_0303;
		}
	}
	{
		float L_51 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		CharacterMotorJumping_t2963212952 * L_52 = __this->get_jumping_9();
		NullCheck(L_52);
		float L_53 = L_52->get_lastButtonDownTime_8();
		if ((((float)((float)il2cpp_codegen_subtract((float)L_51, (float)L_53))) >= ((float)(0.2f))))
		{
			goto IL_0303;
		}
	}
	{
		// grounded = false;
		__this->set_grounded_12((bool)0);
		// jumping.jumping = true;
		CharacterMotorJumping_t2963212952 * L_54 = __this->get_jumping_9();
		NullCheck(L_54);
		L_54->set_jumping_5((bool)1);
		// jumping.lastStartTime = Time.time;
		CharacterMotorJumping_t2963212952 * L_55 = __this->get_jumping_9();
		float L_56 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_55);
		L_55->set_lastStartTime_7(L_56);
		// jumping.lastButtonDownTime = -100;
		CharacterMotorJumping_t2963212952 * L_57 = __this->get_jumping_9();
		NullCheck(L_57);
		L_57->set_lastButtonDownTime_8((((float)((float)((int32_t)-100)))));
		// jumping.holdingJumpButton = true;
		CharacterMotorJumping_t2963212952 * L_58 = __this->get_jumping_9();
		NullCheck(L_58);
		L_58->set_holdingJumpButton_6((bool)1);
		// if (TooSteep())
		bool L_59 = VirtFuncInvoker0< bool >::Invoke(14 /* System.Boolean CharacterMotor::TooSteep() */, __this);
		if (!L_59)
		{
			goto IL_0233;
		}
	}
	{
		// jumping.jumpDir = Vector3.Slerp(Vector3.up, groundNormal, jumping.steepPerpAmount);
		CharacterMotorJumping_t2963212952 * L_60 = __this->get_jumping_9();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_61 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_62 = __this->get_groundNormal_13();
		CharacterMotorJumping_t2963212952 * L_63 = __this->get_jumping_9();
		NullCheck(L_63);
		float L_64 = L_63->get_steepPerpAmount_4();
		Vector3_t3722313464  L_65 = Vector3_Slerp_m802114822(NULL /*static, unused*/, L_61, L_62, L_64, /*hidden argument*/NULL);
		NullCheck(L_60);
		L_60->set_jumpDir_9(L_65);
		goto IL_0259;
	}

IL_0233:
	{
		// jumping.jumpDir = Vector3.Slerp(Vector3.up, groundNormal, jumping.perpAmount);
		CharacterMotorJumping_t2963212952 * L_66 = __this->get_jumping_9();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_67 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_68 = __this->get_groundNormal_13();
		CharacterMotorJumping_t2963212952 * L_69 = __this->get_jumping_9();
		NullCheck(L_69);
		float L_70 = L_69->get_perpAmount_3();
		Vector3_t3722313464  L_71 = Vector3_Slerp_m802114822(NULL /*static, unused*/, L_67, L_68, L_70, /*hidden argument*/NULL);
		NullCheck(L_66);
		L_66->set_jumpDir_9(L_71);
	}

IL_0259:
	{
		// velocity.y = 0;
		(&___velocity0)->set_y_3((((float)((float)0))));
		// velocity += jumping.jumpDir * CalculateJumpVerticalSpeed (jumping.baseHeight);
		Vector3_t3722313464  L_72 = ___velocity0;
		CharacterMotorJumping_t2963212952 * L_73 = __this->get_jumping_9();
		NullCheck(L_73);
		Vector3_t3722313464  L_74 = L_73->get_jumpDir_9();
		CharacterMotorJumping_t2963212952 * L_75 = __this->get_jumping_9();
		NullCheck(L_75);
		float L_76 = L_75->get_baseHeight_1();
		float L_77 = VirtFuncInvoker1< float, float >::Invoke(9 /* System.Single CharacterMotor::CalculateJumpVerticalSpeed(System.Single) */, __this, L_76);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_78 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_74, L_77, /*hidden argument*/NULL);
		Vector3_t3722313464  L_79 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_72, L_78, /*hidden argument*/NULL);
		___velocity0 = L_79;
		// if (movingPlatform.enabled &&
		CharacterMotorMovingPlatform_t3582163828 * L_80 = __this->get_movingPlatform_10();
		NullCheck(L_80);
		bool L_81 = L_80->get_enabled_0();
		if (!L_81)
		{
			goto IL_02f2;
		}
	}
	{
		CharacterMotorMovingPlatform_t3582163828 * L_82 = __this->get_movingPlatform_10();
		NullCheck(L_82);
		int32_t L_83 = L_82->get_movementTransfer_1();
		if ((((int32_t)L_83) == ((int32_t)1)))
		{
			goto IL_02c5;
		}
	}
	{
		CharacterMotorMovingPlatform_t3582163828 * L_84 = __this->get_movingPlatform_10();
		NullCheck(L_84);
		int32_t L_85 = L_84->get_movementTransfer_1();
		if ((!(((uint32_t)L_85) == ((uint32_t)2))))
		{
			goto IL_02f2;
		}
	}

IL_02c5:
	{
		// movement.frameVelocity = movingPlatform.platformVelocity;
		CharacterMotorMovement_t2204346181 * L_86 = __this->get_movement_8();
		CharacterMotorMovingPlatform_t3582163828 * L_87 = __this->get_movingPlatform_10();
		NullCheck(L_87);
		Vector3_t3722313464  L_88 = L_87->get_platformVelocity_9();
		NullCheck(L_86);
		L_86->set_frameVelocity_10(L_88);
		// velocity += movingPlatform.platformVelocity;
		Vector3_t3722313464  L_89 = ___velocity0;
		CharacterMotorMovingPlatform_t3582163828 * L_90 = __this->get_movingPlatform_10();
		NullCheck(L_90);
		Vector3_t3722313464  L_91 = L_90->get_platformVelocity_9();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_92 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_89, L_91, /*hidden argument*/NULL);
		___velocity0 = L_92;
	}

IL_02f2:
	{
		// SendMessage("OnJump", SendMessageOptions.DontRequireReceiver);
		Component_SendMessage_m1441147224(__this, _stringLiteral919726132, 1, /*hidden argument*/NULL);
		goto IL_030f;
	}

IL_0303:
	{
		// jumping.holdingJumpButton = false;
		CharacterMotorJumping_t2963212952 * L_93 = __this->get_jumping_9();
		NullCheck(L_93);
		L_93->set_holdingJumpButton_6((bool)0);
	}

IL_030f:
	{
		// return velocity;
		Vector3_t3722313464  L_94 = ___velocity0;
		return L_94;
	}
}
// System.Void CharacterMotor::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C" IL2CPP_METHOD_ATTR void CharacterMotor_OnControllerColliderHit_m1457311545 (CharacterMotor_t1911343696 * __this, ControllerColliderHit_t240592346 * ___hit0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_OnControllerColliderHit_m1457311545_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		// if (hit.normal.y > 0 && hit.normal.y > groundNormal.y && hit.moveDirection.y < 0) {
		ControllerColliderHit_t240592346 * L_0 = ___hit0;
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = ControllerColliderHit_get_normal_m2699436127(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_y_3();
		if ((((float)L_2) <= ((float)(((float)((float)0))))))
		{
			goto IL_00d9;
		}
	}
	{
		ControllerColliderHit_t240592346 * L_3 = ___hit0;
		NullCheck(L_3);
		Vector3_t3722313464  L_4 = ControllerColliderHit_get_normal_m2699436127(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = (&V_1)->get_y_3();
		Vector3_t3722313464 * L_6 = __this->get_address_of_groundNormal_13();
		float L_7 = L_6->get_y_3();
		if ((((float)L_5) <= ((float)L_7)))
		{
			goto IL_00d9;
		}
	}
	{
		ControllerColliderHit_t240592346 * L_8 = ___hit0;
		NullCheck(L_8);
		Vector3_t3722313464  L_9 = ControllerColliderHit_get_moveDirection_m1770146420(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		float L_10 = (&V_2)->get_y_3();
		if ((((float)L_10) >= ((float)(((float)((float)0))))))
		{
			goto IL_00d9;
		}
	}
	{
		// if ((hit.point - movement.lastHitPoint).sqrMagnitude > 0.001 || lastGroundNormal == Vector3.zero)
		ControllerColliderHit_t240592346 * L_11 = ___hit0;
		NullCheck(L_11);
		Vector3_t3722313464  L_12 = ControllerColliderHit_get_point_m1769704767(L_11, /*hidden argument*/NULL);
		CharacterMotorMovement_t2204346181 * L_13 = __this->get_movement_8();
		NullCheck(L_13);
		Vector3_t3722313464  L_14 = L_13->get_lastHitPoint_12();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_15 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		float L_16 = Vector3_get_sqrMagnitude_m1474274574((Vector3_t3722313464 *)(&V_3), /*hidden argument*/NULL);
		if ((((float)L_16) > ((float)(0.001f))))
		{
			goto IL_0085;
		}
	}
	{
		Vector3_t3722313464  L_17 = __this->get_lastGroundNormal_14();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_18 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_19 = Vector3_op_Equality_m4231250055(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0096;
		}
	}

IL_0085:
	{
		// groundNormal = hit.normal;
		ControllerColliderHit_t240592346 * L_20 = ___hit0;
		NullCheck(L_20);
		Vector3_t3722313464  L_21 = ControllerColliderHit_get_normal_m2699436127(L_20, /*hidden argument*/NULL);
		__this->set_groundNormal_13(L_21);
		goto IL_00a2;
	}

IL_0096:
	{
		// groundNormal = lastGroundNormal;
		Vector3_t3722313464  L_22 = __this->get_lastGroundNormal_14();
		__this->set_groundNormal_13(L_22);
	}

IL_00a2:
	{
		// movingPlatform.hitPlatform = hit.collider.transform;
		CharacterMotorMovingPlatform_t3582163828 * L_23 = __this->get_movingPlatform_10();
		ControllerColliderHit_t240592346 * L_24 = ___hit0;
		NullCheck(L_24);
		Collider_t1773347010 * L_25 = ControllerColliderHit_get_collider_m2639586580(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_t3600365921 * L_26 = Component_get_transform_m3162698980(L_25, /*hidden argument*/NULL);
		NullCheck(L_23);
		L_23->set_hitPlatform_2(L_26);
		// movement.hitPoint = hit.point;
		CharacterMotorMovement_t2204346181 * L_27 = __this->get_movement_8();
		ControllerColliderHit_t240592346 * L_28 = ___hit0;
		NullCheck(L_28);
		Vector3_t3722313464  L_29 = ControllerColliderHit_get_point_m1769704767(L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		L_27->set_hitPoint_11(L_29);
		// movement.frameVelocity = Vector3.zero;
		CharacterMotorMovement_t2204346181 * L_30 = __this->get_movement_8();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_31 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_30);
		L_30->set_frameVelocity_10(L_31);
	}

IL_00d9:
	{
		return;
	}
}
// System.Collections.IEnumerator CharacterMotor::SubtractNewPlatformVelocity()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* CharacterMotor_SubtractNewPlatformVelocity_m2460956088 (CharacterMotor_t1911343696 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_SubtractNewPlatformVelocity_m2460956088_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private function SubtractNewPlatformVelocity () {
		U24SubtractNewPlatformVelocityU241_t3033866676 * L_0 = (U24SubtractNewPlatformVelocityU241_t3033866676 *)il2cpp_codegen_object_new(U24SubtractNewPlatformVelocityU241_t3033866676_il2cpp_TypeInfo_var);
		U24SubtractNewPlatformVelocityU241__ctor_m2198776343(L_0, __this, /*hidden argument*/NULL);
		NullCheck(L_0);
		RuntimeObject* L_1 = U24SubtractNewPlatformVelocityU241_GetEnumerator_m3891665188(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean CharacterMotor::MoveWithPlatform()
extern "C" IL2CPP_METHOD_ATTR bool CharacterMotor_MoveWithPlatform_m2230124238 (CharacterMotor_t1911343696 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_MoveWithPlatform_m2230124238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	bool G_B1_0 = false;
	bool G_B2_0 = false;
	int32_t G_B5_0 = 0;
	int32_t G_B4_0 = 0;
	{
		// return (
		CharacterMotorMovingPlatform_t3582163828 * L_0 = __this->get_movingPlatform_10();
		NullCheck(L_0);
		bool L_1 = L_0->get_enabled_0();
		bool L_2 = L_1;
		G_B1_0 = L_2;
		if (!L_2)
		{
			G_B3_0 = ((int32_t)(L_2));
			goto IL_002d;
		}
	}
	{
		bool L_3 = __this->get_grounded_12();
		bool L_4 = L_3;
		G_B2_0 = L_4;
		if (L_4)
		{
			G_B3_0 = ((int32_t)(L_4));
			goto IL_002d;
		}
	}
	{
		CharacterMotorMovingPlatform_t3582163828 * L_5 = __this->get_movingPlatform_10();
		NullCheck(L_5);
		int32_t L_6 = L_5->get_movementTransfer_1();
		G_B3_0 = ((((int32_t)L_6) == ((int32_t)3))? 1 : 0);
	}

IL_002d:
	{
		int32_t L_7 = G_B3_0;
		G_B4_0 = L_7;
		if (!L_7)
		{
			G_B5_0 = L_7;
			goto IL_0045;
		}
	}
	{
		CharacterMotorMovingPlatform_t3582163828 * L_8 = __this->get_movingPlatform_10();
		NullCheck(L_8);
		Transform_t3600365921 * L_9 = L_8->get_activePlatform_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_9, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_10));
	}

IL_0045:
	{
		return (bool)G_B5_0;
	}
}
// UnityEngine.Vector3 CharacterMotor::GetDesiredHorizontalVelocity()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  CharacterMotor_GetDesiredHorizontalVelocity_m2565923170 (CharacterMotor_t1911343696 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_GetDesiredHorizontalVelocity_m2565923170_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		// var desiredLocalDirection : Vector3 = tr.InverseTransformDirection(inputMoveDirection);
		Transform_t3600365921 * L_0 = __this->get_tr_15();
		Vector3_t3722313464  L_1 = __this->get_inputMoveDirection_6();
		NullCheck(L_0);
		Vector3_t3722313464  L_2 = Transform_InverseTransformDirection_m3843238577(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		// var maxSpeed : float = MaxSpeedInDirection(desiredLocalDirection);
		Vector3_t3722313464  L_3 = V_0;
		float L_4 = VirtFuncInvoker1< float, Vector3_t3722313464  >::Invoke(17 /* System.Single CharacterMotor::MaxSpeedInDirection(UnityEngine.Vector3) */, __this, L_3);
		V_1 = L_4;
		// if (grounded) {
		bool L_5 = __this->get_grounded_12();
		if (!L_5)
		{
			goto IL_005d;
		}
	}
	{
		// var movementSlopeAngle = Mathf.Asin(movement.velocity.normalized.y)  * Mathf.Rad2Deg;
		CharacterMotorMovement_t2204346181 * L_6 = __this->get_movement_8();
		NullCheck(L_6);
		Vector3_t3722313464 * L_7 = L_6->get_address_of_velocity_9();
		Vector3_t3722313464  L_8 = Vector3_get_normalized_m2454957984((Vector3_t3722313464 *)L_7, /*hidden argument*/NULL);
		V_3 = L_8;
		float L_9 = (&V_3)->get_y_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_10 = asinf(L_9);
		V_2 = ((float)il2cpp_codegen_multiply((float)L_10, (float)(57.29578f)));
		// maxSpeed *= movement.slopeSpeedMultiplier.Evaluate(movementSlopeAngle);
		float L_11 = V_1;
		CharacterMotorMovement_t2204346181 * L_12 = __this->get_movement_8();
		NullCheck(L_12);
		AnimationCurve_t3046754366 * L_13 = L_12->get_slopeSpeedMultiplier_3();
		float L_14 = V_2;
		NullCheck(L_13);
		float L_15 = AnimationCurve_Evaluate_m2125563588(L_13, L_14, /*hidden argument*/NULL);
		V_1 = ((float)il2cpp_codegen_multiply((float)L_11, (float)L_15));
	}

IL_005d:
	{
		// return tr.TransformDirection(desiredLocalDirection * maxSpeed);
		Transform_t3600365921 * L_16 = __this->get_tr_15();
		Vector3_t3722313464  L_17 = V_0;
		float L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_19 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t3722313464  L_20 = Transform_TransformDirection_m3784028109(L_16, L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// UnityEngine.Vector3 CharacterMotor::AdjustGroundVelocityToNormal(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  CharacterMotor_AdjustGroundVelocityToNormal_m1559675676 (CharacterMotor_t1911343696 * __this, Vector3_t3722313464  ___hVelocity0, Vector3_t3722313464  ___groundNormal1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_AdjustGroundVelocityToNormal_m1559675676_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		// var sideways : Vector3 = Vector3.Cross(Vector3.up, hVelocity);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_0 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = ___hVelocity0;
		Vector3_t3722313464  L_2 = Vector3_Cross_m418170344(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		// return Vector3.Cross(sideways, groundNormal).normalized * hVelocity.magnitude;
		Vector3_t3722313464  L_3 = V_0;
		Vector3_t3722313464  L_4 = ___groundNormal1;
		Vector3_t3722313464  L_5 = Vector3_Cross_m418170344(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Vector3_t3722313464  L_6 = Vector3_get_normalized_m2454957984((Vector3_t3722313464 *)(&V_1), /*hidden argument*/NULL);
		float L_7 = Vector3_get_magnitude_m27958459((Vector3_t3722313464 *)(&___hVelocity0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_8 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean CharacterMotor::IsGroundedTest()
extern "C" IL2CPP_METHOD_ATTR bool CharacterMotor_IsGroundedTest_m3364471484 (CharacterMotor_t1911343696 * __this, const RuntimeMethod* method)
{
	{
		// return (groundNormal.y > 0.01);
		Vector3_t3722313464 * L_0 = __this->get_address_of_groundNormal_13();
		float L_1 = L_0->get_y_3();
		return (bool)((((float)L_1) > ((float)(0.01f)))? 1 : 0);
	}
}
// System.Single CharacterMotor::GetMaxAcceleration(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR float CharacterMotor_GetMaxAcceleration_m3859506419 (CharacterMotor_t1911343696 * __this, bool ___grounded0, const RuntimeMethod* method)
{
	float G_B3_0 = 0.0f;
	{
		// if (grounded)
		bool L_0 = ___grounded0;
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		// return movement.maxGroundAcceleration;
		CharacterMotorMovement_t2204346181 * L_1 = __this->get_movement_8();
		NullCheck(L_1);
		float L_2 = L_1->get_maxGroundAcceleration_4();
		G_B3_0 = L_2;
		goto IL_0026;
	}

IL_0016:
	{
		// return movement.maxAirAcceleration;
		CharacterMotorMovement_t2204346181 * L_3 = __this->get_movement_8();
		NullCheck(L_3);
		float L_4 = L_3->get_maxAirAcceleration_5();
		G_B3_0 = L_4;
		goto IL_0026;
	}

IL_0026:
	{
		return G_B3_0;
	}
}
// System.Single CharacterMotor::CalculateJumpVerticalSpeed(System.Single)
extern "C" IL2CPP_METHOD_ATTR float CharacterMotor_CalculateJumpVerticalSpeed_m4078330566 (CharacterMotor_t1911343696 * __this, float ___targetJumpHeight0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_CalculateJumpVerticalSpeed_m4078330566_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Mathf.Sqrt (2 * targetJumpHeight * movement.gravity);
		float L_0 = ___targetJumpHeight0;
		CharacterMotorMovement_t2204346181 * L_1 = __this->get_movement_8();
		NullCheck(L_1);
		float L_2 = L_1->get_gravity_6();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_3 = sqrtf(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(((float)((float)2))), (float)L_0)), (float)L_2)));
		return L_3;
	}
}
// System.Boolean CharacterMotor::IsJumping()
extern "C" IL2CPP_METHOD_ATTR bool CharacterMotor_IsJumping_m3606504852 (CharacterMotor_t1911343696 * __this, const RuntimeMethod* method)
{
	{
		// return jumping.jumping;
		CharacterMotorJumping_t2963212952 * L_0 = __this->get_jumping_9();
		NullCheck(L_0);
		bool L_1 = L_0->get_jumping_5();
		return L_1;
	}
}
// System.Boolean CharacterMotor::IsSliding()
extern "C" IL2CPP_METHOD_ATTR bool CharacterMotor_IsSliding_m2746236169 (CharacterMotor_t1911343696 * __this, const RuntimeMethod* method)
{
	bool G_B2_0 = false;
	bool G_B1_0 = false;
	bool G_B4_0 = false;
	bool G_B3_0 = false;
	{
		// return (grounded && sliding.enabled && TooSteep());
		bool L_0 = __this->get_grounded_12();
		bool L_1 = L_0;
		G_B1_0 = L_1;
		if (!L_1)
		{
			G_B2_0 = L_1;
			goto IL_0018;
		}
	}
	{
		CharacterMotorSliding_t602719019 * L_2 = __this->get_sliding_11();
		NullCheck(L_2);
		bool L_3 = L_2->get_enabled_0();
		G_B2_0 = L_3;
	}

IL_0018:
	{
		bool L_4 = G_B2_0;
		G_B3_0 = L_4;
		if (!L_4)
		{
			G_B4_0 = L_4;
			goto IL_0025;
		}
	}
	{
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(14 /* System.Boolean CharacterMotor::TooSteep() */, __this);
		G_B4_0 = L_5;
	}

IL_0025:
	{
		return G_B4_0;
	}
}
// System.Boolean CharacterMotor::IsTouchingCeiling()
extern "C" IL2CPP_METHOD_ATTR bool CharacterMotor_IsTouchingCeiling_m1916692979 (CharacterMotor_t1911343696 * __this, const RuntimeMethod* method)
{
	{
		// return (movement.collisionFlags & CollisionFlags.CollidedAbove) != 0;
		CharacterMotorMovement_t2204346181 * L_0 = __this->get_movement_8();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_collisionFlags_8();
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_1&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean CharacterMotor::IsGrounded()
extern "C" IL2CPP_METHOD_ATTR bool CharacterMotor_IsGrounded_m399426143 (CharacterMotor_t1911343696 * __this, const RuntimeMethod* method)
{
	{
		// return grounded;
		bool L_0 = __this->get_grounded_12();
		return L_0;
	}
}
// System.Boolean CharacterMotor::TooSteep()
extern "C" IL2CPP_METHOD_ATTR bool CharacterMotor_TooSteep_m1639249891 (CharacterMotor_t1911343696 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_TooSteep_m1639249891_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return (groundNormal.y <= Mathf.Cos(controller.slopeLimit * Mathf.Deg2Rad));
		Vector3_t3722313464 * L_0 = __this->get_address_of_groundNormal_13();
		float L_1 = L_0->get_y_3();
		CharacterController_t1138636865 * L_2 = __this->get_controller_16();
		NullCheck(L_2);
		float L_3 = CharacterController_get_slopeLimit_m485529875(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_4 = cosf(((float)il2cpp_codegen_multiply((float)L_3, (float)(0.0174532924f))));
		return (bool)((((int32_t)((((float)L_1) > ((float)L_4))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.Vector3 CharacterMotor::GetDirection()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  CharacterMotor_GetDirection_m232325600 (CharacterMotor_t1911343696 * __this, const RuntimeMethod* method)
{
	{
		// return inputMoveDirection;
		Vector3_t3722313464  L_0 = __this->get_inputMoveDirection_6();
		return L_0;
	}
}
// System.Void CharacterMotor::SetControllable(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void CharacterMotor_SetControllable_m2803857061 (CharacterMotor_t1911343696 * __this, bool ___controllable0, const RuntimeMethod* method)
{
	{
		// canControl = controllable;
		bool L_0 = ___controllable0;
		__this->set_canControl_4(L_0);
		return;
	}
}
// System.Single CharacterMotor::MaxSpeedInDirection(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR float CharacterMotor_MaxSpeedInDirection_m14659612 (CharacterMotor_t1911343696 * __this, Vector3_t3722313464  ___desiredMovementDirection0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_MaxSpeedInDirection_m14659612_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float G_B6_0 = 0.0f;
	float G_B5_0 = 0.0f;
	{
		// if (desiredMovementDirection == Vector3.zero)
		Vector3_t3722313464  L_0 = ___desiredMovementDirection0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_1 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_2 = Vector3_op_Equality_m4231250055(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		// return 0;
		G_B6_0 = (((float)((float)0)));
		goto IL_00ac;
	}

IL_0017:
	{
		// var zAxisEllipseMultiplier : float = (desiredMovementDirection.z > 0 ? movement.maxForwardSpeed : movement.maxBackwardsSpeed) / movement.maxSidewaysSpeed;
		float L_3 = (&___desiredMovementDirection0)->get_z_4();
		if ((((float)L_3) <= ((float)(((float)((float)0))))))
		{
			goto IL_0039;
		}
	}
	{
		CharacterMotorMovement_t2204346181 * L_4 = __this->get_movement_8();
		NullCheck(L_4);
		float L_5 = L_4->get_maxForwardSpeed_0();
		G_B5_0 = L_5;
		goto IL_0044;
	}

IL_0039:
	{
		CharacterMotorMovement_t2204346181 * L_6 = __this->get_movement_8();
		NullCheck(L_6);
		float L_7 = L_6->get_maxBackwardsSpeed_2();
		G_B5_0 = L_7;
	}

IL_0044:
	{
		CharacterMotorMovement_t2204346181 * L_8 = __this->get_movement_8();
		NullCheck(L_8);
		float L_9 = L_8->get_maxSidewaysSpeed_1();
		V_0 = ((float)((float)G_B5_0/(float)L_9));
		// var temp : Vector3 = new Vector3(desiredMovementDirection.x, 0, desiredMovementDirection.z / zAxisEllipseMultiplier).normalized;
		float L_10 = (&___desiredMovementDirection0)->get_x_2();
		float L_11 = (&___desiredMovementDirection0)->get_z_4();
		float L_12 = V_0;
		Vector3_t3722313464  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector3__ctor_m3353183577((&L_13), L_10, (((float)((float)0))), ((float)((float)L_11/(float)L_12)), /*hidden argument*/NULL);
		V_3 = L_13;
		Vector3_t3722313464  L_14 = Vector3_get_normalized_m2454957984((Vector3_t3722313464 *)(&V_3), /*hidden argument*/NULL);
		V_1 = L_14;
		// var length : float = new Vector3(temp.x, 0, temp.z * zAxisEllipseMultiplier).magnitude * movement.maxSidewaysSpeed;
		float L_15 = (&V_1)->get_x_2();
		float L_16 = (&V_1)->get_z_4();
		float L_17 = V_0;
		Vector3_t3722313464  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m3353183577((&L_18), L_15, (((float)((float)0))), ((float)il2cpp_codegen_multiply((float)L_16, (float)L_17)), /*hidden argument*/NULL);
		V_4 = L_18;
		float L_19 = Vector3_get_magnitude_m27958459((Vector3_t3722313464 *)(&V_4), /*hidden argument*/NULL);
		CharacterMotorMovement_t2204346181 * L_20 = __this->get_movement_8();
		NullCheck(L_20);
		float L_21 = L_20->get_maxSidewaysSpeed_1();
		V_2 = ((float)il2cpp_codegen_multiply((float)L_19, (float)L_21));
		// return length;
		float L_22 = V_2;
		G_B6_0 = L_22;
		goto IL_00ac;
	}

IL_00ac:
	{
		return G_B6_0;
	}
}
// System.Void CharacterMotor::SetVelocity(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void CharacterMotor_SetVelocity_m726788142 (CharacterMotor_t1911343696 * __this, Vector3_t3722313464  ___velocity0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_SetVelocity_m726788142_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// grounded = false;
		__this->set_grounded_12((bool)0);
		// movement.velocity = velocity;
		CharacterMotorMovement_t2204346181 * L_0 = __this->get_movement_8();
		Vector3_t3722313464  L_1 = ___velocity0;
		NullCheck(L_0);
		L_0->set_velocity_9(L_1);
		// movement.frameVelocity = Vector3.zero;
		CharacterMotorMovement_t2204346181 * L_2 = __this->get_movement_8();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_frameVelocity_10(L_3);
		// SendMessage("OnExternalVelocity");
		Component_SendMessage_m3172125788(__this, _stringLiteral1959008414, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CharacterMotor::Main()
extern "C" IL2CPP_METHOD_ATTR void CharacterMotor_Main_m513198750 (CharacterMotor_t1911343696 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CharacterMotor/$SubtractNewPlatformVelocity$1::.ctor(CharacterMotor)
extern "C" IL2CPP_METHOD_ATTR void U24SubtractNewPlatformVelocityU241__ctor_m2198776343 (U24SubtractNewPlatformVelocityU241_t3033866676 * __this, CharacterMotor_t1911343696 * ___self_0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24SubtractNewPlatformVelocityU241__ctor_m2198776343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private function SubtractNewPlatformVelocity () {
		GenericGenerator_1__ctor_m599230948(__this, /*hidden argument*/GenericGenerator_1__ctor_m599230948_RuntimeMethod_var);
		CharacterMotor_t1911343696 * L_0 = ___self_0;
		__this->set_U24self_U244_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Object> CharacterMotor/$SubtractNewPlatformVelocity$1::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* U24SubtractNewPlatformVelocityU241_GetEnumerator_m3891665188 (U24SubtractNewPlatformVelocityU241_t3033866676 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24SubtractNewPlatformVelocityU241_GetEnumerator_m3891665188_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private function SubtractNewPlatformVelocity () {
		CharacterMotor_t1911343696 * L_0 = __this->get_U24self_U244_0();
		U24_t1451678995 * L_1 = (U24_t1451678995 *)il2cpp_codegen_object_new(U24_t1451678995_il2cpp_TypeInfo_var);
		U24__ctor_m4161687318(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CharacterMotor/$SubtractNewPlatformVelocity$1/$::.ctor(CharacterMotor)
extern "C" IL2CPP_METHOD_ATTR void U24__ctor_m4161687318 (U24_t1451678995 * __this, CharacterMotor_t1911343696 * ___self_0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24__ctor_m4161687318_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m496418942(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m496418942_RuntimeMethod_var);
		CharacterMotor_t1911343696 * L_0 = ___self_0;
		__this->set_U24self_U243_3(L_0);
		return;
	}
}
// System.Boolean CharacterMotor/$SubtractNewPlatformVelocity$1/$::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U24_MoveNext_m2202740849 (U24_t1451678995 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24_MoveNext_m2202740849_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B13_0 = 0;
	{
		int32_t L_0 = ((GenericGeneratorEnumerator_1_t3544741914 *)__this)->get__state_1();
		switch (L_0)
		{
			case 0:
			{
				goto IL_001f;
			}
			case 1:
			{
				goto IL_012c;
			}
			case 2:
			{
				goto IL_009c;
			}
			case 3:
			{
				goto IL_00ad;
			}
			case 4:
			{
				goto IL_00ef;
			}
		}
	}

IL_001f:
	{
		// if (movingPlatform.enabled &&
		CharacterMotor_t1911343696 * L_1 = __this->get_U24self_U243_3();
		NullCheck(L_1);
		CharacterMotorMovingPlatform_t3582163828 * L_2 = L_1->get_movingPlatform_10();
		NullCheck(L_2);
		bool L_3 = L_2->get_enabled_0();
		if (!L_3)
		{
			goto IL_0124;
		}
	}
	{
		CharacterMotor_t1911343696 * L_4 = __this->get_U24self_U243_3();
		NullCheck(L_4);
		CharacterMotorMovingPlatform_t3582163828 * L_5 = L_4->get_movingPlatform_10();
		NullCheck(L_5);
		int32_t L_6 = L_5->get_movementTransfer_1();
		if ((((int32_t)L_6) == ((int32_t)1)))
		{
			goto IL_0060;
		}
	}
	{
		CharacterMotor_t1911343696 * L_7 = __this->get_U24self_U243_3();
		NullCheck(L_7);
		CharacterMotorMovingPlatform_t3582163828 * L_8 = L_7->get_movingPlatform_10();
		NullCheck(L_8);
		int32_t L_9 = L_8->get_movementTransfer_1();
		if ((!(((uint32_t)L_9) == ((uint32_t)2))))
		{
			goto IL_0124;
		}
	}

IL_0060:
	{
		// if (movingPlatform.newPlatform) {
		CharacterMotor_t1911343696 * L_10 = __this->get_U24self_U243_3();
		NullCheck(L_10);
		CharacterMotorMovingPlatform_t3582163828 * L_11 = L_10->get_movingPlatform_10();
		NullCheck(L_11);
		bool L_12 = L_11->get_newPlatform_10();
		if (!L_12)
		{
			goto IL_00ef;
		}
	}
	{
		// var platform : Transform = movingPlatform.activePlatform;
		CharacterMotor_t1911343696 * L_13 = __this->get_U24self_U243_3();
		NullCheck(L_13);
		CharacterMotorMovingPlatform_t3582163828 * L_14 = L_13->get_movingPlatform_10();
		NullCheck(L_14);
		Transform_t3600365921 * L_15 = L_14->get_activePlatform_3();
		__this->set_U24platformU242_2(L_15);
		// yield WaitForFixedUpdate();
		WaitForFixedUpdate_t1634918743 * L_16 = (WaitForFixedUpdate_t1634918743 *)il2cpp_codegen_object_new(WaitForFixedUpdate_t1634918743_il2cpp_TypeInfo_var);
		WaitForFixedUpdate__ctor_m590323305(L_16, /*hidden argument*/NULL);
		bool L_17 = GenericGeneratorEnumerator_1_Yield_m2645948398(__this, 2, L_16, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m2645948398_RuntimeMethod_var);
		G_B13_0 = ((int32_t)(L_17));
		goto IL_012d;
	}

IL_009c:
	{
		// yield WaitForFixedUpdate();
		WaitForFixedUpdate_t1634918743 * L_18 = (WaitForFixedUpdate_t1634918743 *)il2cpp_codegen_object_new(WaitForFixedUpdate_t1634918743_il2cpp_TypeInfo_var);
		WaitForFixedUpdate__ctor_m590323305(L_18, /*hidden argument*/NULL);
		bool L_19 = GenericGeneratorEnumerator_1_Yield_m2645948398(__this, 3, L_18, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m2645948398_RuntimeMethod_var);
		G_B13_0 = ((int32_t)(L_19));
		goto IL_012d;
	}

IL_00ad:
	{
		// if (grounded && platform == movingPlatform.activePlatform)
		CharacterMotor_t1911343696 * L_20 = __this->get_U24self_U243_3();
		NullCheck(L_20);
		bool L_21 = L_20->get_grounded_12();
		if (!L_21)
		{
			goto IL_00ef;
		}
	}
	{
		Transform_t3600365921 * L_22 = __this->get_U24platformU242_2();
		CharacterMotor_t1911343696 * L_23 = __this->get_U24self_U243_3();
		NullCheck(L_23);
		CharacterMotorMovingPlatform_t3582163828 * L_24 = L_23->get_movingPlatform_10();
		NullCheck(L_24);
		Transform_t3600365921 * L_25 = L_24->get_activePlatform_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_26 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_22, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00ef;
		}
	}
	{
		// yield 1;
		int32_t L_27 = 1;
		RuntimeObject * L_28 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_27);
		bool L_29 = GenericGeneratorEnumerator_1_Yield_m2645948398(__this, 4, L_28, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m2645948398_RuntimeMethod_var);
		G_B13_0 = ((int32_t)(L_29));
		goto IL_012d;
	}

IL_00ef:
	{
		// movement.velocity -= movingPlatform.platformVelocity;
		CharacterMotor_t1911343696 * L_30 = __this->get_U24self_U243_3();
		NullCheck(L_30);
		CharacterMotorMovement_t2204346181 * L_31 = L_30->get_movement_8();
		CharacterMotor_t1911343696 * L_32 = __this->get_U24self_U243_3();
		NullCheck(L_32);
		CharacterMotorMovement_t2204346181 * L_33 = L_32->get_movement_8();
		NullCheck(L_33);
		Vector3_t3722313464  L_34 = L_33->get_velocity_9();
		CharacterMotor_t1911343696 * L_35 = __this->get_U24self_U243_3();
		NullCheck(L_35);
		CharacterMotorMovingPlatform_t3582163828 * L_36 = L_35->get_movingPlatform_10();
		NullCheck(L_36);
		Vector3_t3722313464  L_37 = L_36->get_platformVelocity_9();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_38 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_34, L_37, /*hidden argument*/NULL);
		NullCheck(L_31);
		L_31->set_velocity_9(L_38);
	}

IL_0124:
	{
		// private function SubtractNewPlatformVelocity () {
		GenericGeneratorEnumerator_1_YieldDefault_m4013619751(__this, 1, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m4013619751_RuntimeMethod_var);
	}

IL_012c:
	{
		G_B13_0 = 0;
	}

IL_012d:
	{
		return (bool)G_B13_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CharacterMotorJumping::.ctor()
extern "C" IL2CPP_METHOD_ATTR void CharacterMotorJumping__ctor_m2578071375 (CharacterMotorJumping_t2963212952 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotorJumping__ctor_m2578071375_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// class CharacterMotorJumping {
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		// var enabled : boolean = true;
		__this->set_enabled_0((bool)1);
		// var baseHeight : float = 1.0;
		__this->set_baseHeight_1((1.0f));
		// var extraHeight : float = 4.1;
		__this->set_extraHeight_2((4.1f));
		// var steepPerpAmount : float = 0.5;
		__this->set_steepPerpAmount_4((0.5f));
		// var lastButtonDownTime : float = -100;
		__this->set_lastButtonDownTime_8((((float)((float)((int32_t)-100)))));
		// var jumpDir : Vector3 = Vector3.up;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_0 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_jumpDir_9(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CharacterMotorMovement::.ctor()
extern "C" IL2CPP_METHOD_ATTR void CharacterMotorMovement__ctor_m1785114476 (CharacterMotorMovement_t2204346181 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotorMovement__ctor_m1785114476_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// class CharacterMotorMovement {
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		// var maxForwardSpeed : float = 10.0;
		__this->set_maxForwardSpeed_0((10.0f));
		// var maxSidewaysSpeed : float = 10.0;
		__this->set_maxSidewaysSpeed_1((10.0f));
		// var maxBackwardsSpeed : float = 10.0;
		__this->set_maxBackwardsSpeed_2((10.0f));
		// var slopeSpeedMultiplier : AnimationCurve = AnimationCurve(Keyframe(-90, 1), Keyframe(0, 1), Keyframe(90, 0));
		KeyframeU5BU5D_t1068524471* L_0 = (KeyframeU5BU5D_t1068524471*)SZArrayNew(KeyframeU5BU5D_t1068524471_il2cpp_TypeInfo_var, (uint32_t)3);
		KeyframeU5BU5D_t1068524471* L_1 = L_0;
		NullCheck(L_1);
		Keyframe_t4206410242  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Keyframe__ctor_m391431887((&L_2), (((float)((float)((int32_t)-90)))), (((float)((float)1))), /*hidden argument*/NULL);
		*(Keyframe_t4206410242 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_2;
		KeyframeU5BU5D_t1068524471* L_3 = L_1;
		NullCheck(L_3);
		Keyframe_t4206410242  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Keyframe__ctor_m391431887((&L_4), (((float)((float)0))), (((float)((float)1))), /*hidden argument*/NULL);
		*(Keyframe_t4206410242 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_4;
		KeyframeU5BU5D_t1068524471* L_5 = L_3;
		NullCheck(L_5);
		Keyframe_t4206410242  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Keyframe__ctor_m391431887((&L_6), (((float)((float)((int32_t)90)))), (((float)((float)0))), /*hidden argument*/NULL);
		*(Keyframe_t4206410242 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_6;
		AnimationCurve_t3046754366 * L_7 = (AnimationCurve_t3046754366 *)il2cpp_codegen_object_new(AnimationCurve_t3046754366_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m1565662948(L_7, L_5, /*hidden argument*/NULL);
		__this->set_slopeSpeedMultiplier_3(L_7);
		// var maxGroundAcceleration : float = 30.0;
		__this->set_maxGroundAcceleration_4((30.0f));
		// var maxAirAcceleration : float = 20.0;
		__this->set_maxAirAcceleration_5((20.0f));
		// var gravity : float = 10.0;
		__this->set_gravity_6((10.0f));
		// var maxFallSpeed : float = 20.0;
		__this->set_maxFallSpeed_7((20.0f));
		// var frameVelocity : Vector3 = Vector3.zero;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_8 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_frameVelocity_10(L_8);
		// var hitPoint : Vector3 = Vector3.zero;
		Vector3_t3722313464  L_9 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_hitPoint_11(L_9);
		// var lastHitPoint : Vector3 = Vector3(Mathf.Infinity, 0, 0);
		Vector3_t3722313464  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m3353183577((&L_10), (std::numeric_limits<float>::infinity()), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		__this->set_lastHitPoint_12(L_10);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CharacterMotorMovingPlatform::.ctor()
extern "C" IL2CPP_METHOD_ATTR void CharacterMotorMovingPlatform__ctor_m3446355503 (CharacterMotorMovingPlatform_t3582163828 * __this, const RuntimeMethod* method)
{
	{
		// class CharacterMotorMovingPlatform {
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		// var enabled : boolean = true;
		__this->set_enabled_0((bool)1);
		// var movementTransfer : MovementTransferOnJump = MovementTransferOnJump.PermaTransfer;
		__this->set_movementTransfer_1(2);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CharacterMotorSliding::.ctor()
extern "C" IL2CPP_METHOD_ATTR void CharacterMotorSliding__ctor_m1828951945 (CharacterMotorSliding_t602719019 * __this, const RuntimeMethod* method)
{
	{
		// class CharacterMotorSliding {
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		// var enabled : boolean = true;
		__this->set_enabled_0((bool)1);
		// var slidingSpeed : float = 15;
		__this->set_slidingSpeed_1((((float)((float)((int32_t)15)))));
		// var sidewaysControl : float = 1.0;
		__this->set_sidewaysControl_2((1.0f));
		// var speedControl : float = 0.4;
		__this->set_speedControl_3((0.4f));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FPSInputController::.ctor()
extern "C" IL2CPP_METHOD_ATTR void FPSInputController__ctor_m3295241438 (FPSInputController_t1143113571 * __this, const RuntimeMethod* method)
{
	{
		// private var motor : CharacterMotor;
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FPSInputController::Awake()
extern "C" IL2CPP_METHOD_ATTR void FPSInputController_Awake_m2261530822 (FPSInputController_t1143113571 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FPSInputController_Awake_m2261530822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Cursor.visible = false;
		Cursor_set_visible_m2693238713(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		// motor = GetComponent(CharacterMotor);
		RuntimeTypeHandle_t3027515415  L_0 = { reinterpret_cast<intptr_t> (CharacterMotor_t1911343696_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Component_t1923634451 * L_2 = Component_GetComponent_m886226392(__this, L_1, /*hidden argument*/NULL);
		__this->set_motor_4(((CharacterMotor_t1911343696 *)CastclassClass((RuntimeObject*)L_2, CharacterMotor_t1911343696_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FPSInputController::Update()
extern "C" IL2CPP_METHOD_ATTR void FPSInputController_Update_m457713114 (FPSInputController_t1143113571 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FPSInputController_Update_m457713114_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		// var directionVector = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_0 = Input_GetAxis_m4009438427(NULL /*static, unused*/, _stringLiteral1828639942, /*hidden argument*/NULL);
		float L_1 = Input_GetAxis_m4009438427(NULL /*static, unused*/, _stringLiteral2984908384, /*hidden argument*/NULL);
		Vector3_t3722313464  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m3353183577((&L_2), L_0, (((float)((float)0))), L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		// if (directionVector != Vector3.zero) {
		Vector3_t3722313464  L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_4 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_5 = Vector3_op_Inequality_m315980366(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0051;
		}
	}
	{
		// var directionLength = directionVector.magnitude;
		float L_6 = Vector3_get_magnitude_m27958459((Vector3_t3722313464 *)(&V_0), /*hidden argument*/NULL);
		V_1 = L_6;
		// directionVector = directionVector / directionLength;
		Vector3_t3722313464  L_7 = V_0;
		float L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_9 = Vector3_op_Division_m510815599(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		// directionLength = Mathf.Min(1, directionLength);
		float L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_11 = Mathf_Min_m1073399594(NULL /*static, unused*/, (((float)((float)1))), L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		// directionLength = directionLength * directionLength;
		float L_12 = V_1;
		float L_13 = V_1;
		V_1 = ((float)il2cpp_codegen_multiply((float)L_12, (float)L_13));
		// directionVector = directionVector * directionLength;
		Vector3_t3722313464  L_14 = V_0;
		float L_15 = V_1;
		Vector3_t3722313464  L_16 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		V_0 = L_16;
	}

IL_0051:
	{
		// motor.inputMoveDirection = transform.rotation * directionVector;
		CharacterMotor_t1911343696 * L_17 = __this->get_motor_4();
		Transform_t3600365921 * L_18 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Quaternion_t2301928331  L_19 = Transform_get_rotation_m3502953881(L_18, /*hidden argument*/NULL);
		Vector3_t3722313464  L_20 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_21 = Quaternion_op_Multiply_m2607404835(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		NullCheck(L_17);
		L_17->set_inputMoveDirection_6(L_21);
		// motor.inputJump = Input.GetButton("Jump");
		CharacterMotor_t1911343696 * L_22 = __this->get_motor_4();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_23 = Input_GetButton_m2064261504(NULL /*static, unused*/, _stringLiteral1930566815, /*hidden argument*/NULL);
		NullCheck(L_22);
		L_22->set_inputJump_7(L_23);
		return;
	}
}
// System.Void FPSInputController::Main()
extern "C" IL2CPP_METHOD_ATTR void FPSInputController_Main_m3205165190 (FPSInputController_t1143113571 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
